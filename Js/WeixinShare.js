﻿/*!
 * 微信分享朋友圈或好友群日志
 * 
 */


function getQueryString(name) {
	var result = window.location.search.match(new RegExp("[\?\&]" + name + "=([^\&]+)", "i"));
	if (result == null || result.length < 1) {
		return "";
	}
	return result[1];
}

function shareLog(openid, urlfile, share_type) {
	$.ajax({
		type:"post",
		dataType:"json",
		contentType:"application/json;charset:utf-8",
		data:JSON.stringify({"open_id":openid,"url":urlfile,"share_type":share_type}),
		url:"http://www.yourweb.com/Weixin/ArticleRec.ashx",
		success:function(data){
			console.log(data);
		}
	})
}

function shareWeixin(title_desc, shareimg) {
	var current_url = location.href.split('#')[0];
	//var nickname = getQueryString("nickname");
	var openid = getQueryString("open_id");
	//var openid=$.getUrlParam('open_id');
	var current_title = document.title;
	var urlfile = current_url.split('?');
	urlfile = urlfile[0].substring(urlfile[0].lastIndexOf("/") + 1);
	

	$.ajax({
		type:"post",
		dataType:"json",
		contentType:"application/json;charset:utf-8",
		data:JSON.stringify({"url":current_url}),
		url:"http://www.yourweb.com/Weixin/GetParaData.ashx",
		success:function(data)
		{ 
			console.log(data);
			wx.config({
				debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
				appId: 'xxxxxxxxxxxxx', // 必填，公众号的唯一标识
				timestamp: data.timestamp, // 必填，生成签名的时间戳
				nonceStr: data.nonceStr, // 必填，生成签名的随机串
				signature: data.signature,// 必填，签名
				jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage'] // 必填，需要使用的JS接口列表
			});
				
			wx.ready(function() {
				
				wx.onMenuShareTimeline({
					title: current_title, // 分享标题   getNickName(nickname) + 
					link: "http://www.yourweb.com/Weixin/ModelSecdir.aspx?openid="+ openid + "&urlfile=" + urlfile, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
					imgUrl: shareimg , // 分享图标
					success: function () {
						// 用户确认分享后执行的回调函数
						shareLog(openid, urlfile, "分享朋友圈");
					},
					cancel: function () {
						// 用户取消分享后执行的回调函数
					}
				});
				wx.onMenuShareAppMessage({
					title: current_title, // 分享标题  getNickName(nickname)
					desc: title_desc,  // 分享描述，此文字结尾必须加英文版逗号
					link: "http://www.yourweb.com/Weixin/ModelSecdir.aspx?openid="+ openid + "&urlfile=" + urlfile, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
					//link: current_url,
					imgUrl: shareimg , // 分享图标
					type: '', // 分享类型,music、video或link，不填默认为link
					dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
					success: function () {
						// 用户确认分享后执行的回调函数
						shareLog(openid, urlfile, "发送好友或群");
					},
					cancel: function () {
						// 用户取消分享后执行的回调函数
					}
				});
			})
		}
	})
}

