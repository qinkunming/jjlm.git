

--用户关注记录表
CREATE TABLE weixin_join_log
(
	open_id nvarchar(40),   --公众号内微信用户唯一编码
	join_time datetime,     --关注时间
	remove_flag int         --取消关注标志
)


--用户取消关注记录表
CREATE TABLE weixin_leave_log
(
	open_id nvarchar(40),   --公众号内微信用户唯一编码
	leave_time datetime     --取消关注时间
)



--公众号粉丝用户信息表
CREATE TABLE weixin_user_info
(
	open_id nvarchar(40) NULL,          --公众号内微信用户唯一编码
	nickname nvarchar(60) NULL,         --昵称
	sex int NULL,                       --性别
	language nvarchar(20) NULL,         --语言
	city nvarchar(40) NULL,             --城市
	province nvarchar(60) NULL,         --省分
	country nvarchar(60) NULL,          --乡镇
	headimgurl nvarchar(400) NULL,      --头像图片链接
	unionid nvarchar(60) NULL,          --公众号、小程序、网页扫码登录共用的用户唯一标识
	remark nvarchar(20) NULL,           --备注
	groupid nvarchar(20) NULL,          --用户所在编组
	subscribe_scene nvarchar(60) NULL,  --关注来源
	qr_scene nvarchar(10) NULL,         --扫码类型
	qr_scene_str nvarchar(100) NULL,    --扫码类型描述
	remove_flag nvarchar(10) NULL,      --是否取消关注
	latitude nvarchar(50) NULL,         --GPS纬度
	longitude nvarchar(50) NULL,        --GPS经度
	precision nvarchar(50) NULL,        --GPS精度
	addr_label nvarchar(200) NULL,      --GPS位置地址描述
	guishu_openid nvarchar(40) NULL,    --推荐人openid
	guishu_mobile nvarchar(12) NULL,    --推荐人手机号
	guishu_type nvarchar(20) NULL,      --推荐来源类型
	join_time datetime NULL,            --关注时间
	personal_score bigint NULL          --用户积分
)


--公众号接收到微信用户的普通消息
create table weixin_recv_msg
(
	msg_type varchar(30),              --消息类型
	msg_id varchar(40),                --消息编号
	open_id varchar(40),               --微信用户的openid
	gh_id varchar(30),                 --公众号原始id
	recv_time datetime,                --接收时间
	text_content ntext,                --微信用户发送的文本内容
	pic_url ntext,                     --微信用户发送的图片链接
	media_id varchar(40),              --微信用户发送的媒体链接，微信服务器专用链接（含语音、视频、段视频）
	voice_format varchar(20),          --语音消息格式
	voice_recognition varchar(40),     --语音消息转译后的文字
	video_thumb_media_id varchar(40),  --视频消息、段视频消息的缩略图链接
	location_x nvarchar(50),           --位置消息纬度
	location_y nvarchar(50),           --位置消息经度
	location_scale nvarchar(50),       --位置消息精度
	location_label varchar(200),       --位置消息详细地址
	link_title varchar(300),           --链接消息标题
	link_description ntext,            --链接消息描述
	link_url ntext                     --链接消息超链接
)


--公众号接收到微信用户的事件消息
create table weixin_recv_event
(
	msg_type varchar(30),              --消息类型	
    event_type varchar(30),            --事件类型
	open_id varchar(40),               --微信用户的openid
	gh_id varchar(30),                 --公众号原始id
	recv_time datetime,                --接收时间
    event_key varchar(30),             --事件参数
    ticket varchar(200),                --带参数二维码编号
	latitude nvarchar(50),             --位置消息纬度
	longitude nvarchar(50),            --位置消息经度
	precision nvarchar(50)             --位置消息精度
)


--公众号菜单设置表
CREATE TABLE weixin_menu
(
	caidan_id int,               --菜单id
	caidan_name nvarchar(20),    --菜单名称
	caidan_type nvarchar(10),    --菜单类型
	parentid int,                --父菜单id
	index_level int,             --层级水平
	lu_or_ek nvarchar(500),      --菜单所转向的链接
	createtime datetime,         --创建时间
	isuse int                    --是否在用，0为在用，1为失效
) 



--带参数二维码配置表
CREATE TABLE weixin_qrcode_ticket
(
	scene_id int,                      --带参数二维码id，用于查找二维码图片
	create_time datetime,              --从微信服务器获取二维码时间
	qrcode_image_url nvarchar(100),    --从微信服务器获取的二维码保存位置，生成自己的url
	distribute_time datetime,          --二维码分配给推广人、合伙人的时间
	open_id nvarchar(40),              --推广人、合伙人的openid
	scan_image_url nvarchar(100),      --扫码返回图文的logo图片链接
	scan_title nvarchar(100),          --扫码返回图文的文字标题
	scan_title_sub nvarchar(100),      --扫码返回图文的子标题
	scan_redirect_url nvarchar(100),   --扫码后如果直接跳转链接，导向新链接的网址
	red_packet_flag nvarchar(4),       --是否给扫码的微信用户发红包
	red_packet_min float,              --给扫码的微信用户发红包的随机值最小值
	red_packet_max float,              --给扫码的微信用户发红包的随机值最大值
	r_red_packet_flag nvarchar(4),     -是否给推广人发红包
	r_red_packet_min float,            --给推广人发红包的随机值最小值
	r_red_packet_max float,            --给推广人发红包的随机值最小值
	logo_image_name nvarchar(100),     --二维码对应商铺的LOGO
	shop_name nvarchar(100),           --二维码对应商铺的名称
	shop_id nvarchar(12),              --二维码对应商铺的id
	user_id nvarchar(20),              --推广人的编码，一般用手机号
	user_name nvarchar(200),           --推广人的姓名
	scan_type nvarchar(20),            --推广活动名称
	scan_text nvarchar(200)            --推广活动简介
)


--微信支付之支出记录表
CREATE TABLE weixin_pay_payment_record
(
	open_id nvarchar(50),            --收款人openid
	payment_class nvarchar(30),      --支付类型，红包、转账到零钱等
	payment_num float,               --支付金额
	payment_time datetime,           --支付时间
	payment_reason nvarchar(200),    --支付缘由
	payment_trade_no nvarchar(50),   --支付订单号
	related_order_no nvarchar(50),   --相关订单号
	remark nvarchar(100)             --备注
)
 
--微信支付之收款记录表
CREATE TABLE weixin_pay_collect_record
(
	open_id nvarchar(50),           --微信用户openid
	nonce_str nvarchar(32),         --微信支付参数
	result_code nvarchar(20),       --支付动作返回结果
	is_subscribe nvarchar(2),       --是否关注公众号，未关注公众号的微信用户也能支付
	trade_type nvarchar(20),        --交易类型
	bank_type nvarchar(20),         --银行类型
	total_fee int NULL,             --交易金额，单位：分
	cash_fee int NULL,              --现金金额
	transaction_id nvarchar(50),    --微信内支付编码
	out_trade_no nvarchar(50),      --应用方自定的交易编码
	time_end nvarchar(20),          --完成时间
	input_time datetime NULL,       --记录生成时间
	mark nvarchar(10)               --备注
) 


--accesstoken获取记录
CREATE TABLE weixin_accesstoken(
	APPID nvarchar(30),
	SECRET nvarchar(100),
	ACCESSTOKEN nvarchar(600),
	get_date datetime NULL
)


--jsapi获取记录
CREATE TABLE weixin_jsapi(
	ticket nvarchar(100),
	get_date datetime NULL
)


--公众号接口消息记录文本格式
CREATE TABLE weixin_msg_log(
	msg_content ntext,
	rcv_time datetime NULL
) TEXTIMAGE_ON PRIMARY


--公众号接口消息记录xml格式
CREATE TABLE weixin_msg_xml_log(
	FromUserName nvarchar(50),
	ToUserName nvarchar(50),
	MsgType nvarchar(50),
	Event1 nvarchar(50),
	EventKey nvarchar(50),
	MsgTime datetime NULL
)

------------------------------------------------------------
--以下表是公众号降价联盟个性化所需，不建也不影响功能开发测试


--短信发送日志
CREATE TABLE sms_send_log(
	recv_phone_number nvarchar(15),
	sms_content nvarchar(200),
	send_time datetime NULL,
	sms_sender nvarchar(20),
	sms_remark nvarchar(100),
	sms_out_id nvarchar(30),
	result_code nvarchar(100)
)


--分享赚红包日志
CREATE TABLE weixin_share_log(
	open_id nvarchar(50),
	share_time datetime NULL,
	product_url nvarchar(150),
	share_type nvarchar(16)
)


--分享赚红包帖子标题清单
CREATE TABLE weixin_share_title(
	title_class varchar(20),
	title_text varchar(200)
)

