﻿/*
*类名：QinMingToolsDB
*归属：QinMing.Config.DB命名空间
*用途：数据库操作工具类
*作者：乱世刀疤
*日期：2020.05.01
*/

using System;
using System.Data;
using System.Data.SqlClient;
using QinMing.Config;

namespace QinMing.Tools.DB
{
    public  class QinMingToolsDB : System.Web.UI.Page
    {
		
		/// <summary>
        /// 执行更新语句
        /// </summary>
		public static void UpdateTable(string sql)
		{
			SqlConnection Conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
			Conn.Open();
			SqlCommand Comm = new SqlCommand();
			Comm.Connection = Conn;
			
			Comm.CommandText = sql;
			Comm.ExecuteScalar();

			if (Conn.State == ConnectionState.Open)
			{
				Conn.Close();
				Conn.Dispose();
			}
		}
		
		/// <summary>
        /// 执行插入语句
        /// </summary>
		public static void InsertTable(string sql)
		{
			SqlConnection Conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
			Conn.Open();
			SqlCommand Comm = new SqlCommand();
			Comm.Connection = Conn;
			
			Comm.CommandText = sql;
			Comm.ExecuteScalar();

			if (Conn.State == ConnectionState.Open)
			{
				Conn.Close();
				Conn.Dispose();
			}
		}
		
		/// <summary>
        /// 数据库记录查询，返回一条记录的某一个字段
        /// </summary>
		public static string SearchOneField(string sql,string FieldName)
		{
			string str="";
			SqlConnection Conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
			Conn.Open();
			SqlCommand Comm = new SqlCommand();
			Comm.Connection = Conn;
			SqlDataReader dr;
			
			Comm.CommandText = sql;
			dr=Comm.ExecuteReader();			
			if(dr.Read())
			{
				str = dr[FieldName].ToString();
			}
			dr.Close();
			
			if (Conn.State == ConnectionState.Open)
			{
				Conn.Close();
				Conn.Dispose();
			}
			
			return str;
		}

	}
}