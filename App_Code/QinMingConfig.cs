﻿/*
*类名：QinMingConfig
*归属：QinMing.Config命名空间
*用途：全局配置参数
*作者：乱世刀疤
*日期：2020.05.01
*/

using System;

namespace QinMing.Config
{
    /// <summary>
    /// Config 的摘要说明
    /// </summary>
    public class QinMingConfig
    {
        public QinMingConfig()
        {
            //
            // TODO: 在此处添加构造函数逻辑
            //
        }

        /// <summary>
        /// Sql Server 数据库连接串
        /// </summary>
        public const string DatabaseConnStr = "Data Source=数据库主机;Initial Catalog=数据库;User ID=用户名;Password=密码";  
		public const string DatabaseConnStrJoke = "Data Source=数据库主机;Initial Catalog=数据库;User ID=用户名;Password=密码"; 

        /// <summary>
        /// 微信公众号接入参数
        /// </summary>
        public const string Weixin_Token = "define";
        public const string Weixin_AppId = "wxd8xxxxxxxxxxxxx";
        public const string Weixin_AppSecret = "xxxxxxxxxxxxxxxxxxxxx";
        public const string EncodingAESKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

        /// <summary>
        /// 微信支付相关参数
        /// </summary>
        public const string Weixin_MchId = "微信支付商户号，10位";
        public const string Weixin_MchIdKey = "xxxxxxxxxxxxxxxxxxxxxxxx";
        public const string TenPay_SSLCERT_PATH = "目录/微信支付网址下载的证书文件.p12";  //暂时未用到
		public const string certPath = @"E:\目录\微信支付网址下载的证书文件.p12";
        public const string Weixin_OperationPassword = "xxxxxxxxxx";

        /// <summary>
        /// 服务器url
        /// </summary>
        public const string Url_WebServer = "http://www.yourweb.com";
        public const string Url_ImageServer = "http://cwlyg.yourimageserver.com";

        /// <summary>
        /// 请求超时设置（以毫秒为单位），默认为10秒。
        /// 说明：此处常量专为提供给方法的参数的默认值，不是方法内所有请求的默认超时时间。
        /// </summary>
        public const int TIME_OUT = 10000;

    }

}
