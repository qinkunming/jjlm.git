﻿/*
*类名：QinMingSms
*归属：QinMing.Sms命名空间
*用途：短信接口，对外公开的类为QinMingSms，内部可以更换为不同的厂家接口
*作者：乱世刀疤
*日期：2020.05.02
*/

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System.Net;
using QinMing.Config;

//以下为class QinMingSmsAliyun所需，用的是阿里云的短信，接口由阿里云提供
using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Exceptions;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using System.Threading;

namespace QinMing.Sms
{
	//对客户统一一个类库，不同短信服务商接口或许不同，修改此类里的核心部分即可，客户层面无需修改代码
	public class QinMingSms
	{
		//发送验证码类短信，一般数量小，内置数据库连接
		public void SendVerificationSms(string SmsRecvPhoneNumber, string SmsContent, string SmsRemark)
		{
			string response = "";
			Random r = new Random();
            int i = r.Next(1000, 9999);
			string SmsOutId = DateTime.Now.ToString("yyyyMMddHHmmssms") + i.ToString();

			//调用阿里云短信接口
			QinMingSmsAliyun sms = new QinMingSmsAliyun();
			response = sms.SendVerificationSms(SmsRecvPhoneNumber, SmsContent, SmsOutId);
			
            SqlConnection conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
			cmd.CommandText = "insert into sms_send_log (recv_phone_number,sms_content,send_time,sms_sender,sms_remark,sms_out_id,result_code) values"
			    + "('" + SmsRecvPhoneNumber + "','" + SmsContent + "',getdate(),'降价联盟','" + SmsRemark + "','" + SmsOutId + "','" + response + "')";
            cmd.ExecuteScalar();
			if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                conn.Dispose();
            }
		}
		
		//发送通知类短信，一般批量操作，传递数据库连接，减少数据库连接压力
		public void SendNoticeSms()
		{
			
		}
		
		//发送推广类短信，一般批量操作，传递数据库连接，减少数据库连接压力
		public void SendSpreadSms()
		{
			
		}
	}
	
	public class QinMingSmsAliyun
	{
        //产品名称:云通信短信API产品,开发者无需替换
        const String product = "xxxxxxxxxxx";
        //产品域名,开发者无需替换
        const String domain = "dysmsapi.aliyuncs.com";
        // TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
        const String accessKeyId = "xxxxxxxxxxxxxx";
        const String accessKeySecret = "xxxxxxxxxxxxxxxxxxxx";

		public string SendVerificationSms(string SmsRecvPhoneNumber, string SmsContent, string SmsOutId)
		{
            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();
            SendSmsResponse response = null;
            try
            {
                //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
                request.PhoneNumbers = SmsRecvPhoneNumber;
                //必填:短信签名-可在短信控制台中找到
                request.SignName = "降价联盟";
                //必填:短信模板-可在短信控制台中找到
                request.TemplateCode = "xxxxxxxxx";
                //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                request.TemplateParam = "{\"code\":\"" + SmsContent + "\"}";
                //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
                request.OutId = SmsOutId;
                //请求失败这里会抛ClientException异常
                response = acsClient.GetAcsResponse(request);
            }
            catch 
            {
                
            }
            return(response.Code);
		}
		
	}
}
