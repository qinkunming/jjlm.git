﻿/*
*类名：GetJoke
*归属：QinMing.Joke命名空间
*用途：从笑掉牙齿数据库随机取段子
*作者：乱世刀疤
*日期：2020.05.01
*/

using System;
using System.Data;
using System.Data.SqlClient;
using QinMing.Config;

namespace QinMing.Joke
{
	public class GetJoke
	{
		//随机获取一个段子
		public string GetOneJokeText()
		{
			string response = "";

            SqlConnection conn = new SqlConnection(QinMingConfig.DatabaseConnStrJoke);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
			SqlDataReader dr;
            cmd.Connection = conn;
			cmd.CommandText = "select top 1 * from t_joke where 1=1 ORDER BY NEWID()";
			dr = cmd.ExecuteReader();			
			if(dr.Read())
			{
				response = dr["content"].ToString();
			}
			dr.Close();
			if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                conn.Dispose();
            }
			
			return response;
		}
	}
}
