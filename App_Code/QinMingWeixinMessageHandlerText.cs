﻿/*
*类名：TextMessageDeal
*归属：QinMing.Weixin.MessageHandlerText命名空间
*用途：处理微信用户发给公众号的文本消息，并给予微信用户处理结果通知，通过微信服务器转发给微信用户
*作者：乱世刀疤
*日期：2020.05.02
*/

using System;
using System.Web;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using QinMing.Config;
using QinMing.Weixin.ReturnContent;
using QinMing.Joke;

namespace QinMing.Weixin.MessageHandlerText
{
	//文本消息处理
    public class TextMessageDeal :System.Web.UI.Page
    {

		public string DealResult(string weixinXML)
        {
            string content = DealText(weixinXML);  
            return content;
        }
        
        public string DealText(string weixinXML)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(weixinXML);
            XmlNodeList list = doc.GetElementsByTagName("xml");
            XmlNode xn = list[0];
            string FromUserName = xn.SelectSingleNode("//FromUserName").InnerText;   //关注用户的加密后openid
            string ToUserName = xn.SelectSingleNode("//ToUserName").InnerText;       //公众微信号原始ID
            string MsgType = xn.SelectSingleNode("//MsgType").InnerText;
            string Content = xn.SelectSingleNode("//Content").InnerText.ToString().Trim();
			string MsgId = xn.SelectSingleNode("//MsgId").InnerText;
            string strresponse = "";
            
            //引用QinMing.Weixin.ReturnContent命名空间下的ReturnMsg类来生成固定格式的回应消息
			ReturnMsg rm = new ReturnMsg();
            switch (Content)
            {
                case "合伙人":
                    strresponse = rm.ReturnText(FromUserName, ToUserName, "您好！欢迎加盟降价联盟合伙人！\n\n<a href='http://www.yourweb.com/weixin/youhuiquan/youhuiindex.aspx?open_id=" + FromUserName + "'>点击此处进入推广海报获取页面</a>" );
					break;	
				case "段子":
                    GetJoke joke = new GetJoke();
					strresponse = rm.ReturnText(FromUserName, ToUserName, joke.GetOneJokeText() );
					break;
				case "测试":
                    strresponse = rm.ReturnText(FromUserName, ToUserName, "您刚发送了测试" );
					break;
                default:
                    string content = "";
                    content = "欢迎光临降价联盟。";
                    strresponse = rm.ReturnText(FromUserName, ToUserName, content);
                    break;
            }
			
			//保存文本信息
			SaveText(FromUserName, ToUserName, Content, MsgId);
			
			//告知客服人员有新的留言，或者启动客服消息管理工单流程
			QinMing.WeixinTemplateMessage.SendTemplateMessage.SendRemindMsg("管理员openid", "客户留言：" + Content, "");  
			
            return strresponse; 
        } 
		
		//把用户发的文本信息保存到数据表表中
		public void SaveText(string FromUserName, string ToUserName, string Content, string MsgId)
		{
            SqlConnection conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
			
			cmd.CommandText = "insert into weixin_recv_msg (msg_type,msg_id,open_id,gh_id,recv_time,text_content) values("
			    + "'text','" + MsgId + "','" + FromUserName + "','" + ToUserName + "',getdate(),'" + Content + "')";
            cmd.ExecuteScalar();
			
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                conn.Dispose();
            }
		}
    }
}