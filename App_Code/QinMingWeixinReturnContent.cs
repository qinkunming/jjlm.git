﻿/*
*类名：ReturnMsg
*归属：QinMing.Weixin.ReturnContent命名空间
*用途：把公众号返回给微信用户的信息格式化成微信服务器规定的XML格式，并通过微信服务器转发给微信用户
*作者：乱世刀疤
*日期：2020.05.02
*/

using System;

namespace QinMing.Weixin.ReturnContent
{
    public class ReturnMsg :System.Web.UI.Page
    {
        //向客户返回文本消息
		public string ReturnText(string FromUserName,string ToUserName,string content)
        {
            string strresponse =        "<xml>";
            strresponse = strresponse + "<ToUserName><![CDATA[" + FromUserName + "]]></ToUserName>";
            strresponse = strresponse + "<FromUserName><![CDATA[" + ToUserName + "]]></FromUserName>";
            strresponse = strresponse + "<CreateTime>" + DateTime.Now.Ticks.ToString() + "</CreateTime>";
            strresponse = strresponse + "<MsgType><![CDATA[text]]></MsgType>";
            strresponse = strresponse + "<Content><![CDATA[" + content + "]]></Content>";
            strresponse = strresponse + "</xml>";
            return strresponse;
        }
		
		//向客户返回图片消息
        public string ReturnImage(string FromUserName,string ToUserName,string ImageId)
        {
            string strresponse =        "<xml>";
            strresponse = strresponse + "<ToUserName><![CDATA[" + FromUserName + "]]></ToUserName>";
            strresponse = strresponse + "<FromUserName><![CDATA[" + ToUserName + "]]></FromUserName>";
            strresponse = strresponse + "<CreateTime>" + DateTime.Now.Ticks.ToString() + "</CreateTime>";
            strresponse = strresponse + "<MsgType><![CDATA[image]]></MsgType>";
            strresponse = strresponse + "<Image>";
            strresponse = strresponse + "<MediaId><![CDATA[" + ImageId + "]]></MediaId>";
            strresponse = strresponse + "</Image>";
            strresponse = strresponse + "</xml>";
            return strresponse;
        }
		
		//向客户返回语音消息
        public string ReturnVoice(string FromUserName,string ToUserName,string VoiceId)
        {
            string strresponse =        "<xml>";
            strresponse = strresponse + "<ToUserName><![CDATA[" + FromUserName + "]]></ToUserName>";
            strresponse = strresponse + "<FromUserName><![CDATA[" + ToUserName + "]]></FromUserName>";
            strresponse = strresponse + "<CreateTime>" + DateTime.Now.Ticks.ToString() + "</CreateTime>";
            strresponse = strresponse + "<MsgType><![CDATA[voice]]></MsgType>";
            strresponse = strresponse + "<Voice>";
            strresponse = strresponse + "<MediaId><![CDATA[" + VoiceId + "]]></MediaId>";
            strresponse = strresponse + "</Voice>";
            strresponse = strresponse + "</xml>";
            return strresponse;
        }
		
		//向客户返回视频消息
        public string ReturnVideo(string FromUserName, string ToUserName, string VideoId, string Title, string Description)
        {
            string strresponse =        "<xml>";
            strresponse = strresponse + "<ToUserName><![CDATA[" + FromUserName + "]]></ToUserName>";
            strresponse = strresponse + "<FromUserName><![CDATA[" + ToUserName + "]]></FromUserName>";
            strresponse = strresponse + "<CreateTime>" + DateTime.Now.Ticks.ToString() + "</CreateTime>";
            strresponse = strresponse + "<MsgType><![CDATA[video]]></MsgType>";
            strresponse = strresponse + "<Video>";
            strresponse = strresponse + "<MediaId><![CDATA[" + VideoId + "]]></MediaId>";
			strresponse = strresponse + "<Title><![CDATA[" + Title + "]]></Title>";
            strresponse = strresponse + "<Description><![CDATA[" + Description + "]]></Description>";
            strresponse = strresponse + "</Video>";
            strresponse = strresponse + "</xml>";
            return strresponse;
        }
		
		//向客户返回音乐消息
        public string ReturnMusic(string FromUserName, string ToUserName, string Title, string Description,string MusicUrl, string HQMusicUrl, string MusicId)
        {
            string strresponse =        "<xml>";
            strresponse = strresponse + "<ToUserName><![CDATA[" + FromUserName + "]]></ToUserName>";
            strresponse = strresponse + "<FromUserName><![CDATA[" + ToUserName + "]]></FromUserName>";
            strresponse = strresponse + "<CreateTime>" + DateTime.Now.Ticks.ToString() + "</CreateTime>";
            strresponse = strresponse + "<MsgType><![CDATA[music]]></MsgType>";
            strresponse = strresponse + "<Music>";
	        strresponse = strresponse + "<Title><![CDATA[" + Title + "]]></Title>";
            strresponse = strresponse + "<Description><![CDATA[" + Description + "]]></Description>";
            strresponse = strresponse + "<MusicUrl><![CDATA[" + MusicUrl + "]]></MusicUrl>";
            strresponse = strresponse + "<HQMusicUrl><![CDATA[" + HQMusicUrl + "]]></HQMusicUrl>";
            strresponse = strresponse + "<ThumbMediaId><![CDATA[" + MusicId + "]]></ThumbMediaId>";
            strresponse = strresponse + "</Music>";
            strresponse = strresponse + "</xml>";
            return strresponse;
        }
        
        //向客户返回图文消息，因微信限制，现在只能返回单图文形式
		public string ReturnTuWen(string FromUserName, string ToUserName, string Title, string Description, string PicUrl, string Url)
        {
			string strresponse =        "<xml>";
			strresponse = strresponse + "<ToUserName><![CDATA[" + FromUserName + "]]></ToUserName>";
			strresponse = strresponse + "<FromUserName><![CDATA[" + ToUserName + "]]></FromUserName>";
			strresponse = strresponse + "<CreateTime>" + DateTime.Now.Ticks.ToString() + "</CreateTime>";
			strresponse = strresponse + "<MsgType><![CDATA[news]]></MsgType>";
			strresponse = strresponse + "<ArticleCount>1</ArticleCount>";
			strresponse = strresponse + "<Articles>";
			strresponse = strresponse + "<item>";
			strresponse = strresponse + "<Title><![CDATA["+ Title+ "]]></Title>";
			strresponse = strresponse + "<Description><![CDATA["+ Description +"]]></Description>"; 
			strresponse = strresponse + "<PicUrl><![CDATA["+ PicUrl +"]]></PicUrl>";
			strresponse = strresponse + "<Url><![CDATA["+ Url +"]]></Url>";
			strresponse = strresponse + "</item>"; 
			strresponse = strresponse + "</Articles>";
			strresponse = strresponse + "</xml>";
			return strresponse;
        }

    }
}