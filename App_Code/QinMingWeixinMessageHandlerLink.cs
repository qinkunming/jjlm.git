﻿/*
*类名：LinkMessageDeal
*归属：QinMing.Weixin.MessageHandlerLink命名空间
*用途：处理微信用户发给公众号的链接消息，并给予微信用户处理结果通知，通过微信服务器转发给微信用户
*作者：乱世刀疤
*日期：2020.05.02
*/

using System;
using System.Web;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using QinMing.Config;
using QinMing.Weixin.ReturnContent;

namespace QinMing.Weixin.MessageHandlerLink
{
	//链接消息处理
    public class LinkMessageDeal :System.Web.UI.Page
    {

		public string DealResult(string weixinXML)
        {
            string content = DealLink(weixinXML);  
            return content;
        }

        public string DealLink(string weixinXML)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(weixinXML);
            XmlNodeList list = doc.GetElementsByTagName("xml");
            XmlNode xn = list[0];
            string FromUserName = xn.SelectSingleNode("//FromUserName").InnerText;   //关注用户的加密后openid
            string ToUserName = xn.SelectSingleNode("//ToUserName").InnerText;       //公众微信号原始ID
            string strresponse = "";
			
			//用户通过向公众号发送位置发送链接信息
			string Title = xn.SelectSingleNode("//Title").InnerText;
			string Description = xn.SelectSingleNode("//Description").InnerText;
			string Url = xn.SelectSingleNode("//Url").InnerText;
			string MsgId = xn.SelectSingleNode("//MsgId").InnerText;
			
			//保存链接信息
			SaveLink(FromUserName, ToUserName, Title, Description, Url, MsgId);
			
			ReturnMsg rm = new ReturnMsg();
			strresponse = rm.ReturnText(FromUserName, ToUserName, "您发送的链接信息已收录，感谢您的参与");			
			
			//告知客服人员有微信用户发送链接消息
			QinMing.WeixinTemplateMessage.SendTemplateMessage.SendRemindMsg("管理员openid", "客户发送位置消息", "");  

            return strresponse; 
        }
		
		//把用户发的链接信息保存到数据表表中
		public void SaveLink(string FromUserName, string ToUserName, string Title, string Description , string Url, string MsgId)
		{
            SqlConnection conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
			
			cmd.CommandText = "insert into weixin_recv_msg (msg_type,msg_id,open_id,gh_id,recv_time,link_title,link_description,link_url) values("
			    + "'link','" + MsgId + "','" + FromUserName + "','" + ToUserName + "',getdate(),'" + Title + "','" + Description + "','" + Url + "')";
            cmd.ExecuteScalar();
			
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                conn.Dispose();
            }
		}

    }
}