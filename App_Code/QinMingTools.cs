﻿/*
*类名：QinMingTools
*归属：QinMing.Tools命名空间
*用途：常用工具集合：如判断手机号是否合规、号码是否为数字、sql注入判断、获取周一日期、写入日志、获取客户端ip地址等
*作者：乱世刀疤
*日期：2020.05.02
*/

using System;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json;  
using Newtonsoft.Json.Converters; 
using Newtonsoft.Json.Linq; 
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;  
using Newtonsoft.Json.Converters; 
using Newtonsoft.Json.Linq; 
using QinMing.Config;
using QinMing.WeixinContainer;
using QinMing.WeixinUserInfo; 

namespace QinMing.Tools
{
    public  class QinMingTools : System.Web.UI.Page
    {
		
		/// <summary>
        /// 判断号码是否为11位数字
        /// </summary>
		public static bool CheckTel(string tmpstr)
		{
			Regex mobileReg = new Regex("^[0-9]{11,11}$");
			if (mobileReg.IsMatch(tmpstr))
				return true;
			else
				return false;
		}
		
		/// <summary>
        /// 判断号码是否为数字
        /// </summary>
		public static bool CheckNumber(string tmpstr)
		{
			Regex mobileReg = new Regex("^[0-9]*[1-9][0-9]*$");
			if (mobileReg.IsMatch(tmpstr))
				return true;
			else
				return false;
		}
		
		/// <summary>
        /// 弹窗提醒
        /// </summary>
		public void AlertMsg(string msgstr)
		{
			String tmpstr;
			tmpstr = "<script language=javascript>alert('" + msgstr + "');</script>";
			this.RegisterStartupScript("result", tmpstr);
		}

		/// <summary>
        /// 防止SQL注入
        /// </summary>
		public static bool IsHasSQLInject(string str)
		{
			bool isHasSQLInject = false;//字符串中的关键字更新需要添加
			string inj_str = "'|;|and|exec|union|create|insert|select|delete|update|count|*|%|chr|mid|master|truncate|char|declare|+|(|)";
			if(string.IsNullOrEmpty(str))
			{
				return isHasSQLInject; 
			}
			str = str.ToLower().Trim();
			string[] inj_str_array = inj_str.Split('|');
			foreach (string sql in inj_str_array)
			{
				if (str.IndexOf(sql) > -1)
				{
					isHasSQLInject = true;
					break;
				}
			}
			return isHasSQLInject; 
		}

		/// <summary>
        /// 获取周一日期
        /// </summary>
		public string GetMondayDate()
		{
			int i = Convert.ToInt32(DateTime.Now.DayOfWeek.ToString("d"));
			if (i == 0)
			{
				return DateTime.Now.AddDays(1 - Convert.ToInt32(DateTime.Now.DayOfWeek.ToString("d")) - 7).ToString("yyyyMMdd");

			}
			else return DateTime.Now.AddDays(1 - Convert.ToInt32(DateTime.Now.DayOfWeek.ToString("d"))).ToString("yyyyMMdd");
		}
		
		/**
        * 日志操作
        * @param type 日志记录类型
        * @param content 写入内容
        */
        public static void WriteLog(string type, string content)
        {
			string path = HttpContext.Current.Request.PhysicalApplicationPath + "Logs";
			
            if(!Directory.Exists(path))//如果日志目录不存在就创建
            {
                Directory.CreateDirectory(path);
            }

            string time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");//获取当前系统时间
            string filename = path + "\\sql_ddl"+DateTime.Now.ToString("yyyyMMdd")+".log";//用日期对日志文件命名

            //创建或打开日志文件，向日志文件末尾追加记录
            StreamWriter mySw = File.AppendText(filename); 

            //向日志文件写入内容
            string write_content = time + " " + type + " : " + content;
            mySw.WriteLine(write_content);

            //关闭日志文件
            mySw.Close();
        }
		
		public string GetWebClientIp()  
        {  
            string userIP = "";  
            try  
            {  
                if (System.Web.HttpContext.Current == null  
					|| System.Web.HttpContext.Current.Request == null  
					|| System.Web.HttpContext.Current.Request.ServerVariables == null)  
                    return "";  
                string CustomerIP = "";  
                //CDN加速后取到的IP simone 090805  
                CustomerIP = System.Web.HttpContext.Current.Request.Headers["Cdn-Src-Ip"];  
                if (!string.IsNullOrEmpty(CustomerIP))  
                {  
                    return CustomerIP;  
                }  
                CustomerIP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];  
                if (!String.IsNullOrEmpty(CustomerIP))  
                {  
                    return CustomerIP;  
                }  
                if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)  
                {  
                    CustomerIP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];  
                    if (CustomerIP == null)  
                        CustomerIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];  
                }  
                else  
                {  
                    CustomerIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];  
                }  
                if (string.Compare(CustomerIP, "unknown", true) == 0)  
                    return System.Web.HttpContext.Current.Request.UserHostAddress;  
                return CustomerIP;  
            }  
            catch 
			{ 
			
			}  
            return userIP;  
        }

	}
}