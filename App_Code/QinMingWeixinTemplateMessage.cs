﻿/*
*类名：SendTemplateMessage
*归属：QinMing.WeixinTemplateMessage命名空间
*用途：模板消息类，给微信用户发送模板消息。
*作者：乱世刀疤
*日期：2020.05.08
*/

using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using LitJson;
using System.Net;
using System.IO;
using QinMing.WeixinContainer;
using QinMing.Config;

namespace QinMing.WeixinTemplateMessage
{
    public class SendTemplateMessage
    {

		//以下所有模板消息的template_id都是因公众号而异，要从公众号配置的模板消息中去获取
		
		/// <summary>
        /// 模板消息：派单成功提醒，用于对公众号管理员发送各类提醒信息
        /// </summary>
		public static void SendRemindMsg(string open_id, string msg_title, string redirect_url) 
		{  
			QinMingWeixinContainer gt=new QinMingWeixinContainer();
			string access_token = gt.GetAccessToken();
			string msgid = "";  

			string poster = "{\"touser\": \"" + open_id + "\",\"template_id\":\"Y3-mTYsfrPBA_SzTqQruMLXPzUsCcKH_9eF_kMGE3o0\", "
				+ "\"url\":\"" + redirect_url + "\"," 
				+ "\"data\":{\"first\":{\"value\":\"" + msg_title + "\",\"color\":\"#ff0000\"},"
				+ "\"keyword1\":{\"value\":\"" + DateTime.Now.ToString("yyyyMMddHHmmssms") + "\",\"color\":\"#ff0000\"},"
				+ "\"keyword2\":{\"value\":\"" + DateTime.Now.ToString("yyyy:MM:dd HH:mm:ss") + "\",\"color\":\"#ff0000\"},"
				+ "\"remark\":{\"value\":\"点击这里查看详情。\",\"color\":\"#0000ff\"}"
				+ " } }";  
			string resultStr = GetPage("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token, poster);  

		}

		/// <summary>
        /// 模板消息：新会员或新合伙人注册成功提醒，用于对公众号管理员发送提醒信息
        /// </summary>
		public static void SendNewMemberMsg(string open_id, string mobile, string msg_title, string redirect_url) 
		{  
			QinMingWeixinContainer gt=new QinMingWeixinContainer();
			string access_token = gt.GetAccessToken();
			string msgid = "";  

			string poster = "{\"touser\": \"" + open_id + "\",\"template_id\":\"tQ_vHnFzPSl5TLvOudVeJTDF2ME8yroNhJw2713SMF0\", "
				+ "\"url\":\"" + redirect_url + "\"," 
				+ "\"data\":{\"first\":{\"value\":\"" + msg_title + "\",\"color\":\"#ff0000\"},"
				+ "\"keyword1\":{\"value\":\"" + open_id + "\",\"color\":\"#ff0000\"},"
				+ "\"keyword2\":{\"value\":\"" + mobile + "\",\"color\":\"#ff0000\"},"
				+ "\"keyword3\":{\"value\":\"" + DateTime.Now.ToString("yyyy:MM:dd HH:mm:ss") + "\",\"color\":\"#ff0000\"},"
				+ "\"remark\":{\"value\":\"点击这里查看详情。\",\"color\":\"#0000ff\"}"
				+ " } }";  
			string resultStr = GetPage("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token, poster);  

		}
		
        public static string GetPage(string posturl, string postData)  
        {  
			Stream outstream = null;  
			Stream instream = null;  
			StreamReader sr = null;  
			HttpWebResponse response = null;  
			HttpWebRequest request = null;  
			Encoding encoding = Encoding.UTF8;  
			byte[] data = encoding.GetBytes(postData);  
			// 准备请求...  
			try  
			{  
				// 设置参数  
				request = WebRequest.Create(posturl) as HttpWebRequest;  
				CookieContainer cookieContainer = new CookieContainer();  
				request.CookieContainer = cookieContainer;  
				request.AllowAutoRedirect = true;  
				request.Method = "POST";  
				request.ContentType = "application/x-www-form-urlencoded";  
				request.ContentLength = data.Length;  
				outstream = request.GetRequestStream();  
				outstream.Write(data, 0, data.Length);  
				outstream.Close();  
				//发送请求并获取相应回应数据  
				response = request.GetResponse() as HttpWebResponse;  
				//直到request.GetResponse()程序才开始向目标网页发送Post请求  
				instream = response.GetResponseStream();  
				sr = new StreamReader(instream, encoding);  
				//返回结果网页（html）代码  
				string content = sr.ReadToEnd();  
				string err = string.Empty;  
				return content;  
			}  
			catch (Exception ex)  
			{  
				string err = ex.Message;  
				return string.Empty;  
			}  
        }  


    }

}