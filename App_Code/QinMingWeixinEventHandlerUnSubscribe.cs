﻿/*
*类名：UnSubscribeEventDeal
*归属：QinMing.Weixin.EventHandlerUnSubscribe命名空间
*用途：处理微信用户发给公众号的取消关注公众号事件，并给予微信用户处理结果通知，通过微信服务器转发给微信用户
*作者：乱世刀疤
*日期：2020.05.02
*/

using System;
using System.Web;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using QinMing.Config;
using QinMing.Tools;
using QinMing.Tools.DB;
using QinMing.Weixin.ReturnContent;
using QinMing.WeixinUserInfo;
using QinMing.WeixinPayPayment;

namespace QinMing.Weixin.EventHandlerUnSubscribe
{
	//事件消息处理：取消关注公众号
    public class UnSubscribeEventDeal :System.Web.UI.Page
    {

		public string DealResult(string weixinXML)
        {
            string content = DealSubscribe(weixinXML);  
            return content;
        }

        public string DealSubscribe(string weixinXML)
        {
            string content1="";
            string strresponse = "";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(weixinXML);
            XmlNodeList list = doc.GetElementsByTagName("xml");
            XmlNode xn = list[0];
            string FromUserName = xn.SelectSingleNode("//FromUserName").InnerText;   //关注用户的加密后openid
            string ToUserName = xn.SelectSingleNode("//ToUserName").InnerText;       //公众微信号原始ID
            string MsgType=xn.SelectSingleNode("//MsgType").InnerText;
            string Event=xn.SelectSingleNode("//Event").InnerText;
			string EventKey = xn.SelectSingleNode("//EventKey").InnerText;
            
			DealLeave(FromUserName);
			
			SaveEvent(FromUserName, ToUserName);
			
            //给管理员发送粉丝取消关注通知
			QinMing.WeixinTemplateMessage.SendTemplateMessage.SendRemindMsg("管理员openid", "粉丝取消关注提醒" + FromUserName, "http://www.yourweb.com/Weixin/DisplayOneUser.aspx?open_id=" + FromUserName);  
			
			return strresponse;
        }
		
        //记录粉丝取关公众号时的相关信息
		public void DealLeave(string FromUserName)
        {
            string strConn = QinMingConfig.DatabaseConnStr;
            SqlConnection conn = new SqlConnection(strConn);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "insert into weixin_leave_log (open_id,leave_time) values('" + FromUserName + "',getdate())";
            cmd.ExecuteScalar();
            cmd.CommandText = "update weixin_join_log set remove_flag=1 where remove_flag=0 and open_id='" + FromUserName + "'";
            cmd.ExecuteScalar();
            cmd.CommandText = "update weixin_user_info set remove_flag = '已取关' where open_id = '" + FromUserName + "'";
            cmd.ExecuteScalar();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                conn.Dispose();
            }
        }
		
		//保存事件信息。
        public void SaveEvent(string FromUserName, string ToUserName)
        {
            SqlConnection conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "insert into weixin_recv_event (msg_type,event_type,open_id,gh_id,recv_time) "
			    + "values ('event','unsubscribe','" + FromUserName + "','" + ToUserName + "',getdate()) ";
            //QinMingTools.WriteLog("sql语句：", cmd.CommandText);
			cmd.ExecuteScalar();
      
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                conn.Dispose();
            }
        }

    }
}