﻿/*
*类名：LocationMessageDeal
*归属：QinMing.Weixin.MessageHandlerLocation命名空间
*用途：处理微信用户发给公众号的位置消息，并给予微信用户处理结果通知，通过微信服务器转发给微信用户
*作者：乱世刀疤
*日期：2020.05.02
*/

using System;
using System.Web;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using QinMing.Config;
using QinMing.Weixin.ReturnContent;

namespace QinMing.Weixin.MessageHandlerLocation
{
	//位置消息处理
    public class LocationMessageDeal :System.Web.UI.Page
    {

		public string DealResult(string weixinXML)
        {
            string content = DealLocation(weixinXML);  
            return content;
        }

        public string DealLocation(string weixinXML)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(weixinXML);
            XmlNodeList list = doc.GetElementsByTagName("xml");
            XmlNode xn = list[0];
            string FromUserName = xn.SelectSingleNode("//FromUserName").InnerText;   //关注用户的加密后openid
            string ToUserName = xn.SelectSingleNode("//ToUserName").InnerText;       //公众微信号原始ID
            string strresponse = "";
			
			//用户通过向公众号发送位置上报地理位置信息
			string Latitude = xn.SelectSingleNode("//Location_X").InnerText;
			string Longitude = xn.SelectSingleNode("//Location_Y").InnerText;
			string ScaleW = xn.SelectSingleNode("//Scale").InnerText;
			string AddrLabel = xn.SelectSingleNode("//Label").InnerText;
			string MsgId = xn.SelectSingleNode("//MsgId").InnerText;
			
			//保存位置信息
			SaveLocation(FromUserName, ToUserName, Latitude, Longitude, ScaleW, AddrLabel, MsgId);
			
			UpdateLocation(FromUserName, Latitude, Longitude, ScaleW, AddrLabel);
			
			ReturnMsg rm = new ReturnMsg();
			strresponse = rm.ReturnText(FromUserName, ToUserName, "您的位置信息已收录，后期如有变更，请重新发送位置。");			
			
			//告知客服人员有微信用户更新位置消息
			QinMing.WeixinTemplateMessage.SendTemplateMessage.SendRemindMsg("管理员openid", "客户发送位置消息", "");  

            return strresponse; 
        }
		
		//把用户发的位置信息保存到数据表表中
		public void SaveLocation(string FromUserName, string ToUserName, string Latitude, string Longitude , string ScaleW, string AddrLabel, string MsgId)
		{
            SqlConnection conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
			
			cmd.CommandText = "insert into weixin_recv_msg (msg_type,msg_id,open_id,gh_id,recv_time,location_x,location_y,location_scale,location_label) values("
			    + "'location','" + MsgId + "','" + FromUserName + "','" + ToUserName + "',getdate(),'" + Latitude + "','" + Longitude + "','" + ScaleW + "','" + AddrLabel + "')";
            cmd.ExecuteScalar();
			
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                conn.Dispose();
            }
		}
		
		//更新微信用户表中的位置经纬度信息
		public void UpdateLocation(string FromUserName, string Latitude, string Longitude , string ScaleW, string AddrLabel)
		{
            SqlConnection conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
			
			cmd.CommandText = "update weixin_user_info set latitude='" + Latitude + "',longitude='" + Longitude + "',"
			    + "precision='" + ScaleW + "',addr_label='" + AddrLabel + "' where open_id='" + FromUserName + "'";
            cmd.ExecuteScalar();
			
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                conn.Dispose();
            }
		}

    }
}