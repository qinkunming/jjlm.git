﻿/*
*类名：LocationEventDeal
*归属：QinMing.Weixin.EventHandlerLocation命名空间
*用途：处理微信用户发给公众号的上报位置信息事件，并给予微信用户处理结果通知，通过微信服务器转发给微信用户
*作者：乱世刀疤
*日期：2020.05.02 
*/

using System;
using System.Web;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using QinMing.Config;
using QinMing.Tools;

namespace QinMing.Weixin.EventHandlerLocation
{
	//事件消息处理：上报位置信息
    public class LocationEventDeal :System.Web.UI.Page
    {

		public string DealResult(string weixinXML)
        {
            string content = DealLocation(weixinXML);  
            return content;
        }

        public string DealLocation(string weixinXML)
        {
            string content1="";
            string strresponse = "";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(weixinXML);
            XmlNodeList list = doc.GetElementsByTagName("xml");
            XmlNode xn = list[0];
            string FromUserName = xn.SelectSingleNode("//FromUserName").InnerText;   //关注用户的加密后openid
            string ToUserName = xn.SelectSingleNode("//ToUserName").InnerText;       //公众微信号原始ID
            string MsgType=xn.SelectSingleNode("//MsgType").InnerText;
            string Event=xn.SelectSingleNode("//Event").InnerText;
			string Latitude = xn.SelectSingleNode("//Latitude").InnerText;
			string Longitude = xn.SelectSingleNode("//Longitude").InnerText;
			string Precision = xn.SelectSingleNode("//Precision").InnerText;
			
			//更新微信用户经纬度位置信息
			GetLocation(FromUserName, Latitude, Longitude, Precision);
			
			//保存上报位置信息事件
			SaveEvent(FromUserName, ToUserName, Latitude, Longitude, Precision);
			
            //给管理员发送粉丝上报位置事件通知
			QinMing.WeixinTemplateMessage.SendTemplateMessage.SendRemindMsg("管理员openid", "粉丝上报位置信息提醒" + FromUserName, "http://www.yourweb.com/Weixin/DisplayOneUser.aspx?open_id=" + FromUserName);  
			
			return strresponse;
        }
		
        //进入公众号后，自动提示是否允许使用粉丝地理位置，同意的发此事件，是用户主动向公众号发送位置信息。
        public void GetLocation(string FromUserName, string Latitude, string Longitude , string Precision)
        {
            SqlConnection conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "update weixin_user_info set latitude='" + Latitude + "',longitude='" + Longitude + "',"
                            + "precision='" + Precision + "' where open_id='" + FromUserName + "' and remove_flag = '已关注' ";
            cmd.ExecuteScalar();
      
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                conn.Dispose();
            }
        }
		
		//保存事件信息。
        public void SaveEvent(string FromUserName, string ToUserName,string Latitude, string Longitude , string Precision)
        {
            SqlConnection conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "insert into weixin_recv_event (msg_type,event_type,open_id,gh_id,recv_time,latitude,longitude,precision) "
			    + "values ('event','LOCATION','" + FromUserName + "','" + ToUserName + "',getdate(),'" + Latitude + "','" + Longitude + "','" + Precision + "') ";
            //QinMingTools.WriteLog("sql语句：", cmd.CommandText);
			cmd.ExecuteScalar();
      
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                conn.Dispose();
            }
        }

    }
}