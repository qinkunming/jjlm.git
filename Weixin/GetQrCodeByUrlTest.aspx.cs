﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using LitJson;
using System.Net;
using System.IO;
using QinMing.Config;
using QinMing.WeixinContainer;
using QinMing.Tools;

namespace Jjlm
{
	
	public partial class GetQrCodeByUrlTest : System.Web.UI.Page
	{

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				//GenQrcode(sender, e);
			}
		}

		protected void GenQrcode(object sender, EventArgs e)
		{
			string tmpimgurl = QinMing.Tools.Qrcode.QrcodeTools.GetQrcodeByUrl(tbxUrl.Text, "QrCodeUrl", QinMingConfig.Url_WebServer);
			Image1.ImageUrl = tmpimgurl;
			//Response.Write(QinMing.Tools.Qrcode.QrcodeTools.GetQrcodeImageData(tbxUrl.Text));
		}
		
	}

}