﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="MenuSet.aspx.cs" Inherits="Jjlm.MenuSet" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>微信公众号菜单设置</title>
	<!-- Bootstrap核心css文件 -->
	<link href="../css/bootstrap.css" rel="stylesheet">
</head>

<body bgcolor=#DCDCDC>

<div class="container">
    <form id="form1" runat="server">
        <div align="center">
             <asp:Label id="lblAC" runat="server" align="center" visible="false"/>
        </div>
		
        <table class="table table-bordered table-striped table-hover table-condensed">
           <tr>
               <td width="50%"align="center" colspan="2"><font size="3px" color="green">主菜单设置</font></td>
               <td width="50%"align="center" colspan="2"><font size="3px" color="green">子菜单设置</font></td>
           </tr>
      
           <tr>
               <td rowspan="5">
			        <font size="3px" color="green">主菜单1类型：</font><br/>
                    <font size="3px" color="green">主菜单1名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font>
			   </td> 
               <td rowspan="5">  
			        <asp:DropDownList runat="server" ID="ddCDLX1">
					<asp:ListItem Value="父节点">父节点</asp:ListItem>
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxCN1" runat="server" align="center" /><br/>
					<asp:TextBox id="TbxKOU1" runat="server" align="center" />
                </td> 
                <td width="30%">
				    <font size="3px" color="green">子菜单1类型：</font><br/>
                    <font size="3px" color="green">子菜单1名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font><br/>
                </td> 
                <td width="70%">
					<asp:DropDownList runat="server" ID="ddZCD11LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC11N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ11KOU" runat="server" align="center" width="100%"/><br/>
                </td>
           </tr>
           <tr>
		       <td width="30%">
			        <font size="3px" color="green">子菜单2类型：</font><br/>
                    <font size="3px" color="green">子菜单2名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font><br/>
               </td> 
			   <td width="70%">
			        <asp:DropDownList runat="server" ID="ddZCD12LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC12N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ12KOU" runat="server" align="center" width="100%"/><br/>
               </td>
		   </tr>
           <tr>
		       <td width="30%">
			        <font size="3px" color="green">子菜单3类型：</font><br/>
                    <font size="3px" color="green">子菜单3名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font><br/>
               </td> 
               <td width="70%">
			        <asp:DropDownList runat="server" ID="ddZCD13LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC13N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ13KOU" runat="server" align="center" width="100%"/><br/>
               </td>
           </tr>
           <tr>
               <td width="30%">
			        <font size="3px" color="green">子菜单4类型：</font><br/>
                    <font size="3px" color="green">子菜单4名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font><br/>
               </td> 
               <td width="70%">
			        <asp:DropDownList runat="server" ID="ddZCD14LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC14N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ14KOU" runat="server" align="center" width="100%"/><br/>
			   </td>
           </tr>
           <tr>
		       <td width="30%">
			        <font size="3px" color="green">子菜单5类型：</font><br/>
                    <font size="3px" color="green">子菜单5名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font>
               </td> 
               <td width="70%">
			        <asp:DropDownList runat="server" ID="ddZCD15LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC15N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ15KOU" runat="server" align="center" width="100%"/><br/>
               </td>
           </tr>

           <tr>
               <td rowspan="5">
			        <font size="3px" color="green">主菜单2类型：</font><br/>
                    <font size="3px" color="green">主菜单2名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font>
			   </td> 
               <td rowspan="5">  
			        <asp:DropDownList runat="server" ID="ddCDLX2">
					<asp:ListItem Value="父节点">父节点</asp:ListItem>
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxCN2" runat="server" align="center" /><br/>
					<asp:TextBox id="TbxKOU2" runat="server" align="center" />
               </td> 
               <td width="30%">
			        <font size="3px" color="green">子菜单1类型：</font><br/>
                    <font size="3px" color="green">子菜单1名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font><br/>
               </td> 
               <td width="70%">
					<asp:DropDownList runat="server" ID="ddZCD21LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC21N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ21KOU" runat="server" align="center" width="100%"/><br/>
               </td>
           </tr>
           <tr>
		       <td width="30%">
			        <font size="3px" color="green">子菜单2类型：</font><br/>
                    <font size="3px" color="green">子菜单2名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font><br/>
               </td> 
			   <td width="70%">
			        <asp:DropDownList runat="server" ID="ddZCD22LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC22N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ22KOU" runat="server" align="center" width="100%"/><br/>
               </td>
		   </tr>
           <tr>
		       <td width="30%">
			        <font size="3px" color="green">子菜单3类型：</font><br/>
                    <font size="3px" color="green">子菜单3名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font><br/>
               </td> 
               <td width="70%">
			        <asp:DropDownList runat="server" ID="ddZCD23LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC23N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ23KOU" runat="server" align="center" width="100%"/><br/>
               </td>
           </tr>
           <tr>
               <td width="30%">
			        <font size="3px" color="green">子菜单4类型：</font><br/>
                    <font size="3px" color="green">子菜单4名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font><br/>
               </td> 
               <td width="70%"> 
			        <asp:DropDownList runat="server" ID="ddZCD24LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC24N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ24KOU" runat="server" align="center" width="100%"/><br/>
			   </td>
           </tr>
           <tr>
		       <td width="30%">
			        <font size="3px" color="green">子菜单5类型：</font><br/>
                    <font size="3px" color="green">子菜单5名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font>
               </td> 
               <td width="70%"> 
			        <asp:DropDownList runat="server" ID="ddZCD25LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC25N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ25KOU" runat="server" align="center" width="100%"/><br/>
               </td>
           </tr>

           <tr>
               <td rowspan="5">
			        <font size="3px" color="green">主菜单3类型：</font><br/>
                    <font size="3px" color="green">主菜单3名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font>
			   </td> 
               <td rowspan="5">  
			        <asp:DropDownList runat="server" ID="ddCDLX3">
					<asp:ListItem Value="父节点">父节点</asp:ListItem>
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxCN3" runat="server" align="center" /><br/>
					<asp:TextBox id="TbxKOU3" runat="server" align="center" />
                </td> 
                <td width="30%">
				    <font size="3px" color="green">子菜单1类型：</font><br/>
                    <font size="3px" color="green">子菜单1名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font><br/>
                </td> 
                <td width="70%">
					<asp:DropDownList runat="server" ID="ddZCD31LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC31N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ31KOU" runat="server" align="center" width="100%"/><br/>
                </td>
           </tr>
           <tr>
		       <td width="30%">
			        <font size="3px" color="green">子菜单2类型：</font><br/>
                    <font size="3px" color="green">子菜单2名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font><br/>
               </td> 
			   <td width="70%">
			        <asp:DropDownList runat="server" ID="ddZCD32LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC32N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ32KOU" runat="server" align="center" width="100%"/><br/>
               </td></tr>
           <tr>
		       <td width="30%">
			        <font size="3px" color="green">子菜单3类型：</font><br/>
                    <font size="3px" color="green">子菜单3名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font><br/>
               </td> 
               <td width="70%">
			        <asp:DropDownList runat="server" ID="ddZCD33LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC33N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ33KOU" runat="server" align="center" width="100%"/><br/>
               </td>
           </tr>
           <tr>
               <td width="30%">
			        <font size="3px" color="green">子菜单4类型：</font><br/>
                    <font size="3px" color="green">子菜单4名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font><br/>
               </td> 
               <td width="70%">
			       <asp:DropDownList runat="server" ID="ddZCD34LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC34N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ34KOU" runat="server" align="center" width="100%"/><br/>
			   </td>
           </tr>
           <tr>
		       <td width="30%">
			        <font size="3px" color="green">子菜单5类型：</font><br/>
                    <font size="3px" color="green">子菜单5名称：</font><br/>
                    <font size="3px" color="green">EventKey或者URL：</font>
               </td> 
               <td width="70%">
			        <asp:DropDownList runat="server" ID="ddZCD35LX">
					<asp:ListItem Value="click">click</asp:ListItem>
					<asp:ListItem Value="view">view</asp:ListItem>
					</asp:DropDownList><br/>
					<asp:TextBox id="TbxZC35N" runat="server" align="center" width="100%"/><br/>
					<asp:TextBox id="TbxZ35KOU" runat="server" align="center" width="100%"/><br/>
               </td>
           </tr>
           <tr>
		       <td colspan="4" align="center">
                   <asp:Button ID="btnjson" Runat="server" OnClick="json" Text="生成json发送并返回结果" Visible=true></asp:Button>
               </td>
		   </tr>
       </table>
    </form>
   
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
</div>

</body>

</html>