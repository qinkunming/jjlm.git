﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WeixinPayRecvH5Test.aspx.cs" Inherits="Jjlm.WeixinPayRecvH5Test" %>

<!DOCTYPE html>
<html>
<head runat="server">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <title>微信支付H5收款测试</title>
</head>

<style>
    img{
        width: 100%;
        display: block;
    }
    body{
        margin:0;padding:0;
        background-repeat: no-repeat;
        background-size: 100% 100%;
    }
    .line{
        display: flex;justify-content:space-between;height: 40px;margin: 0 20px;
    }
    .boline{
        border-bottom: 1px solid #ddd;
    }
    .ti{
        font-size: 18px;
        line-height: 40px;
    }
    .text{
        font-size: 16px;
        line-height: 40px;
    }
</style>

<body>
<div style="width: 100%;background:#555;color:#fff;font-size:20px;height: 40px;line-height: 40px;text-align: center;">
    收银台
</div>
<form id="form1" runat="server">
    <div>
        <div style="margin-top:10px;background: #fff;">
            <div class="line boline">
                <span class="ti">订单号:</span>
                <asp:Label runat="server" id="lbBillNo" Text="" Visible="true" class="text" />
            </div>
            <div class="line boline">
                <span class="ti">产品:</span>
                <asp:Label runat="server" id="lbProductName" Text="" Visible="true" class="text" />
            </div>
            <div class="line boline">
                <span class="ti">商品数量: </span>
                <asp:Label runat="server" id="lbProductNum" Text="" Visible="true" class="text" />
            </div>
            <div class="line">
                 <span class="ti">支付金额:</span>
                 <asp:Label runat="server" id="lbTotalFee" Text="" Visible="true" class="text" style="color: crimson;" />
            </div>
        </div>
		<div style="margin: 10px auto;">
			<img src="img/wxzf.jpg" alt="" style="width: 100%;">
        </div>
        <div style="margin: 10px auto;width: 90%;height: 40px;background: #20a91d;border-radius:10px;text-align: center;" runat="server" id="divBtn">
            <asp:Button ID="submit" runat="server" Text="立即支付"  Style="background: #20a91d;border-width:0px;color: #fff;line-height: 35px;font-size: 20px;outline:0px;-webkit-appearance: none;" OnClick="btnCallPayClick"/>
        </div>
        </br>
		
		<asp:Label runat="server" id="lbProductId" Text="" Visible="false" class="text" />
		<asp:Label runat="server" id="lbUrl" Text="" Visible="true" class="text" />

    </div>
</form>
</body>
</html>