﻿using System;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;
using QinMing.Config;
using QinMing.WeixinPayCollect;

namespace Jjlm
{
    
	public partial class WeixinPayRecvH5Test : System.Web.UI.Page
    {
		public static string wxJsApiParam {get;set;} //H5调起JS API参数
		
        protected void Page_Load(object sender, EventArgs e)
        {
            lbProductId.Text = "202102040001";
			lbProductName.Text = "微信支付H5测试";
			lbBillNo.Text = DateTime.Now.ToString("yyyyMMddHHmmssms");   //
			lbProductNum.Text = "1";
			lbTotalFee.Text = "0.01";
        }
		
		protected void btnCallPayClick(object sender, EventArgs e)
        {
			string fee = (Convert.ToDouble(lbTotalFee.Text)*100).ToString(); ///微信单位分
			string out_trade_no = lbBillNo.Text;

            if (lbProductName.Text != null)
            {
                //若传递了相关参数，则调统一下单接口，获得后续相关接口的入口参数
                H5Pay h5Pay = new H5Pay();
				string scip = GetWebClientIp();//获取客户端真实IP
				//string scip = GetIP();//获取客户端真实IP
				string url = h5Pay.GetPayUrl(scip,fee,out_trade_no,lbProductId.Text,lbProductName.Text);
				lbUrl.Text = url;
				Response.Redirect(url, false);//跳转到微信支付中间页 
				//lbUrl.Text = h5Pay.GetPayUrl(scip);
/*
                try
                {
                    
					#region //生成订单
                    SqlConnection Conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
                    Conn.Open();
                    SqlCommand Comm = new SqlCommand();
                    Comm.Connection = Conn;
                    SqlDataReader dr;
                    Comm.CommandText = "select a.*,b.product_limit from xiaoshou_order a\n"+
									   "					left join xiaoshou_product_info b\n"+
                                       "                    on a.product_id = b.product_id\n"+
                                       "where a.re_phone = '" + phone + "' and b.product_limit = '"+ product_limit +"' and  order_status in ('已支付','待收货','已收货')";
                    dr = Comm.ExecuteReader();
                    if (dr.Read() && phone != "15651496061") ///&& phone != "15651496061"
                    {
                        Response.Write("<html><body><script type='text/javascript'>alert('每人限购1次');</script></body></html>");
                        return;
                    }
                    dr.Close();

                    Comm.CommandText = " insert into xiaoshou_order(out_trade_no,openid,re_name,re_phone,re_address,re_quxian,order_time,total_fee,order_status,product_id,share_openid) " +
                        "values('" + out_trade_no + "','H5Paypage','" + uname +"','" + phone + "','"
                        + address +"','" + quxian +"',getdate()," + fee +",'待支付','" + lbproductid.Text + "','H5Paypage')";
                    Comm.ExecuteScalar();
					if (Conn.State == ConnectionState.Open)
                    {
                        Conn.Close();
                        Conn.Dispose();
                    }                    
					#endregion 
					

                    string url = h5Pay.GetPayUrl(scip,fee,out_trade_no,lbProductId.Text,lbProductName.Text);//通过统一下单接口进行H5支付,add parameter
                    //Response.Redirect(url,false);//跳转到微信支付中间页 
					lbUrl.Text = url;

                }
                catch(Exception ex)
                {
                    Log.showlog("zhifu_callpay","下订单失败请返回重试"+ex.ToString());
                    submit.Visible = false;
                }*/
            }
            else
            {
                Log.showlog("zhifu_callpay","页面缺少参数，请返回重试");
            }
        }	
        
        //因H5支付要求商户在统一下单接口中上传用户真实ip地址“spbill_create_ip”，故需要调用以下方法。
        public string GetIP()
        {
            HttpRequest request = HttpContext.Current.Request;
            string result = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(result))
            {
                result = request.ServerVariables["REMOTE_ADDR"];
            }
            if (string.IsNullOrEmpty(result))
            {
                result = request.UserHostAddress;
            }
            if (string.IsNullOrEmpty(result))
            {
                result = "0.0.0.0";
            }
            return result;
        }
		
		public string GetWebClientIp()  
        {  
            string userIP = "";  
            try  
            {  
                if (System.Web.HttpContext.Current == null  
            || System.Web.HttpContext.Current.Request == null  
            || System.Web.HttpContext.Current.Request.ServerVariables == null)  
                    return "";  
                string CustomerIP = "";  
                //CDN加速后取到的IP simone 090805  
                CustomerIP = System.Web.HttpContext.Current.Request.Headers["Cdn-Src-Ip"];  
                if (!string.IsNullOrEmpty(CustomerIP))  
                {  
                    return CustomerIP;  
                }  
                CustomerIP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];  
                if (!String.IsNullOrEmpty(CustomerIP))  
                {  
                    return CustomerIP;  
                }  
                if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)  
                {  
                    CustomerIP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];  
                    if (CustomerIP == null)  
                        CustomerIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];  
                }  
                else  
                {  
                    CustomerIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];  
                }  
                if (string.Compare(CustomerIP, "unknown", true) == 0)  
                    return System.Web.HttpContext.Current.Request.UserHostAddress;  
                return CustomerIP;  
            }  
            catch { }  
            return userIP;  
        }
	
    }
}