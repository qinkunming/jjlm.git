<% @ webhandler language="C#" class="ArticleRec" %>
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Net;
using System.Text;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Newtonsoft.Json;  
using Newtonsoft.Json.Converters; 
using Newtonsoft.Json.Linq;   
using System.Security.Cryptography;
using System.Web.Script.Serialization;
using QinMing.Config;
using QinMing.Tools;

public class ArticleRec : IHttpHandler
{

	public void ProcessRequest(HttpContext ctx)
	{
		StreamReader sr = new StreamReader(ctx.Request.InputStream);
		string stream = sr.ReadToEnd();
		JObject jsonObj = JObject.Parse(stream);
		string url = jsonObj["url"].ToString().Replace("\"","");
		string open_id = jsonObj["open_id"].ToString().Replace("\"","");
		string share_type = jsonObj["share_type"].ToString().Replace("\"","");
		
		//string url = ctx.Request["url"].ToString();
		//string open_id = ctx.Request["open_id"].ToString();
		//string share_type = ctx.Request["share_type"].ToString();
		if(QinMing.Tools.QinMingTools.IsHasSQLInject(url) || QinMing.Tools.QinMingTools.IsHasSQLInject(open_id) || QinMing.Tools.QinMingTools.IsHasSQLInject(share_type))
		{
			ctx.Response.Write("failed");
		}
		else
		{
			SqlConnection Conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
			Conn.Open();
			SqlCommand Comm = new SqlCommand();
			Comm.Connection = Conn;
			Comm.CommandText = "insert into weixin_share_log (open_id,share_time,product_url,share_type) values "
				+ "('" + open_id + "',getdate(),'" + url + "','" + share_type + "')";
			Comm.ExecuteScalar();
			
			if (Conn.State == ConnectionState.Open)
			{
				Conn.Close();
				Conn.Dispose();
			}
			
			ctx.Response.Write("success");
		}
	}
	
	public bool IsReusable
	{
	    get { return true; } 
	}

} 