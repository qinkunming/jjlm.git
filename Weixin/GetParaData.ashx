<% @ webhandler language="C#" class="GetParaData" %>
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.IO;
using System.Net;
using System.Text;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Newtonsoft.Json;  
using Newtonsoft.Json.Converters; 
using Newtonsoft.Json.Linq;   
using System.Security.Cryptography;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using QinMing.WeixinContainer;
using QinMing.Config;

public class GetParaData : IHttpHandler
{
	public void ProcessRequest(HttpContext ctx)
	{
		StreamReader sr = new StreamReader(ctx.Request.InputStream);
		string stream = sr.ReadToEnd();
		JObject jsonObj = JObject.Parse(stream);
		string url = jsonObj["url"].ToString().Replace("\"","");
		QinMingWeixinContainer gt = new QinMingWeixinContainer();
		string jsapi = gt.GetJsapi();
		string timestamp = GetTimeStamp();
		string nonceStr = GetNonceStr();
		HttpContext.Current.Response.ContentType = "text/plain";
		string content = "jsapi_ticket=" + jsapi + "&noncestr=" + nonceStr + "&timestamp=" + timestamp + "&url=" + url;

		string sign = SHA1(content, Encoding.UTF8);
		//sign = sign.ToLower();

		Dictionary<String, String> dicList = new Dictionary<String, String>();
		dicList.Add("timestamp", timestamp);
		dicList.Add("nonceStr", nonceStr);
		dicList.Add("signature", sign);
		string json =(new JavaScriptSerializer()).Serialize(dicList);
		
		ctx.Response.Write(json);
	}
	
	//使用系统自带SHA加密
	public string SHA1(string content, Encoding encode)  
	{  
		try  
		{  
			SHA1 sha1 = new SHA1CryptoServiceProvider();  
			byte[] bytes_in = encode.GetBytes(content);  
			byte[] bytes_out = sha1.ComputeHash(bytes_in);  
			//sha1.Dispose();  
			string result = BitConverter.ToString(bytes_out);  
			result = result.Replace("-", "");  
			return result;  
		}  
		catch (Exception ex)  
		{  
			throw new Exception("SHA1" + ex.Message);  
		}  
	}  
	
	//获取时间戳
	public string GetTimeStamp()  
	{  
		TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);  
		return Convert.ToInt64(ts.TotalSeconds).ToString();  
	}  
	
	//获取随机串
	public string GetNonceStr()  
	{  
		string tmpstr = Guid.NewGuid().ToString("D").Replace("-", "");
		return tmpstr;  
	}  
	
	public bool IsReusable
	{
	    get { return true; } 
	}

} 