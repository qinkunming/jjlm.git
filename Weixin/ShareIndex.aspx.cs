﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using QinMing.Config;
using QinMing.Sms;
using QinMing.Tools;

namespace Jjlm
{
	public partial class ShareIndex : System.Web.UI.Page
	{

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				lbopenid.Text = Request.QueryString["open_id"];  //此处需增加防sql注入判断
				//lbreadopenid.Text = Request.QueryString["read_open_id"];  //此处需增加防sql注入判断

				if(QinMing.Tools.QinMingTools.IsHasSQLInject(lbopenid.Text))
				{
					Response.Write("请勿非法尝试！");
					Response.End();
				}
				
				SqlConnection Conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
				Conn.Open();
				SqlCommand Comm = new SqlCommand();
				Comm.Connection = Conn;
				SqlDataReader dr;
				
				Comm.CommandText = "select * from weixin_share_title order by newid()";
				dr = Comm.ExecuteReader();
				if (dr.Read())
				{
					litTitle.Text = dr["title_text"].ToString();
				}
				dr.Close();
				
				Comm.CommandText = "select qrcode_image_url from weixin_bind_mobile_all a,weixin_qrcode_ticket b "
				    + " where a.mobile=b.user_id and b.scan_type='优惠券' and a.open_id='" + lbopenid.Text + "'";
				dr = Comm.ExecuteReader();
				if (dr.Read())
				{
					imgQrcode1.ImageUrl = "../" + dr["qrcode_image_url"].ToString();
					imgQrcode2.ImageUrl = "../" + dr["qrcode_image_url"].ToString();
					divImage.Visible = true;
					divRemind.Visible = false;
					//hlkJmhhr.NavigateUrl = "PartnerNew.aspx?open_id=" + lbopenid.Text;
				}
				else
				{
					hlkJmhhr.NavigateUrl = "PartnerNew.aspx?open_id=" + lbopenid.Text;
					divRemind.Visible = true;
					divImage.Visible = false;
					litTitle.Text = "友情提醒";
				}
				dr.Close();
				
				if (Conn.State == ConnectionState.Open)
				{
					Conn.Close();
					Conn.Dispose();
				}
			}
		}
		
	}
}