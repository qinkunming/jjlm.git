﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShareIndex.aspx.cs" Inherits="Jjlm.ShareIndex" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="Keywords" content="商家,销售,降价,折扣,打折,优惠,减价,砍价,直降,直销,促销,活动,甩卖,折扣联盟,降价联盟,权益联盟">
	<title><asp:Literal id="litTitle" runat="server"></asp:Literal></title>
	
    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
	<script src="../Js/WeixinShare.js"></script>
	
	<style>
	.imgwidth
	{width:100%;}
	
	.divwidth
	{width:100%;}
	
	.divfenge10
	{height:10px;}
	</style>
</head>

<body onload="shareWeixin('为您推荐的都是最好的！','http://www.yourweb.com/images/logo1.jpg')" style="text-align:center;width:100%;margin:0 auto;background-color: #ffffff;">
    <center>
	<form id="form1" runat="server">
	
	<div runat="server" id="divRemind" >
		<div class="divfenge10"></div>
		<div class="panel panel-default" style="width:95%;">
			<div class="panel-heading">
				<h3 class="panel-title">
					您离红包只差一步之遥
				</h3>
			</div>
			<div class="panel-body">
				因分享需要专用二维码以便记录分享红包归属，所以需要先加盟合伙人以获取个人专属二维码。</br></br>
				<asp:HyperLink ID="hlkJmhhr" runat="server" style="font-size:18px;color:#ff5000;" >点击这里立即加盟合伙人</asp:HyperLink>
			</div>
		</div>
	</div>
	
	<div runat="server" id="divImage" class="divwidth">
	    <img src="http://yourweb.com/tmpimg1/share001.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/share003.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/share002.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/share004.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<div style="text-align:center;color:#ff5000;line-height:30px;font-size:18px;">
		    衣食住行，2折起优惠</br>全国连锁，全国通用</br>微信扫描下方二维码立刻享受
		</div>
		<div class="divfenge10"></div>
		<asp:Image runat="server" id="imgQrcode1" style="width:50%;"></asp:Image>
		<div class="divfenge10"></div>
		<div style="text-align:center;color:#08562b;line-height:30px;font-size:18px;">
		    还有更多优惠商家</br>请继续往下看
		</div>
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/01.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/02.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/03.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/04.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/05.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/06.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/07.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/08.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/09.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<img src="http://yourweb.com/tmpimg1/10.jpg" class="imgwidth" />
		<div class="divfenge10"></div>
		<div style="text-align:center;color:#ff5000;line-height:30px;font-size:18px;">
		    衣食住行，2折起优惠</br>全国连锁，全国通用</br>微信扫描下方二维码立刻享受
		</div>
		<div class="divfenge10"></div>
		<asp:Image runat="server" id="imgQrcode2" style="width:50%;"></asp:Image>
		<div class="divfenge10"></div>
	</div>
	
	<asp:Label runat="server" id="lbResult"/></br></br>
	<asp:Label runat="server" id="lbopenid" Visible="false"/></br>
	<asp:Label runat="server" id="lbreadopenid" Visible="false"/></br>
    </form>
	</center>

	<div style="display:none">
		<script src="js/tongji_baidu.js"></script>
	</div>
	
</body>

</html>
