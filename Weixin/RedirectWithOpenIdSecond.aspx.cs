﻿using System;
using System.Web;
using QinMing.WeixinOAuth;

namespace Jjlm
{
	public partial class RedirectWithOpenIdSecond : System.Web.UI.Page
	{
		
		protected void Page_Load(object sender, EventArgs e)
		{
			string final_url = Request.QueryString["final_url"]; 
			string code = Request.QueryString["code"]; 
			QinMingWeixinOAuth oauth=new QinMingWeixinOAuth();

			//仅获取openid
			string open_id  = oauth.GetOpenId(code);
			Response.Redirect(final_url + "?open_id=" + open_id);
		}

	}
}   

