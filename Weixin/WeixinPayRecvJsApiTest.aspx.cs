﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Threading;
using LitJson;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using QinMing.Config;
using QinMing.WeixinPayCollect;

namespace Jjlm
{
    public partial class WeixinPayRecvJsApiTest : System.Web.UI.Page
    {
        public static string wxJsApiParam {get;set;} //H5调起JS API参数

        protected void Page_Load(object sender, EventArgs e)
        {

			lbBillNo.Text = DateTime.Now.ToString("yyyyMMddHHmmssms");
			lbproductname.Text = "微信支付JsApi收款测试";
			lbopenid.Text =  Request.QueryString["open_id"];          //"orcSBw-hu4YFbm4jN887PZ_8OwLk";  //qkm微信openid

        }

        protected void btnCallPayClick(object sender, EventArgs e)
        {
			string fee = (Convert.ToDouble(lbtotal_fee.Text)*100).ToString(); ///微信单位分
			string out_trade_no = lbBillNo.Text;

			//若传递了相关参数，则调统一下单接口，获得后续相关接口的入口参数
			JsApiPay jsApiPay = new JsApiPay(this);
			jsApiPay.openid = lbopenid.Text;
			jsApiPay.total_fee = int.Parse(fee);

			try
			{
				//使用预先生成好的订单编号out_trade_no
				WxPayData unifiedOrderResult = jsApiPay.GetUnifiedOrderResult(out_trade_no, lbproductname.Text);
				wxJsApiParam = jsApiPay.GetJsApiParameters();//获取H5调起JS API参数                      
				Log.Debug(this.GetType().ToString(), "wxJsApiParam : " + wxJsApiParam);

				//调用前台javascript支付函数
				this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), "", "<script language='javascript'>callpay();</script>", false); ///调用前台callpay（）函数完成支付

				//Thread.Sleep(3000);
				divBtn.Visible = false;   //Enabled
				hlSeeOrder.Visible = true;
				hlSeeOrder.NavigateUrl = "MyOrderList.aspx?openid=" + lbopenid.Text;
			}
			catch(Exception ex)
			{
				Log.showlog("zhifu_callpay","下订单失败请返回重试"+ex.ToString());
				submit.Visible = false;
			}

        }
    }
}