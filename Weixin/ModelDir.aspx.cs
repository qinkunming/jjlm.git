using System;
using System.Web;
using QinMing.WeixinOAuth;
using QinMing.Config;
public partial class ModelDir : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {

        string all = Request.QueryString["urlfile_openid"];
        string[] array=all.Split(new Char[] { '|' });
        
        string openid = array[1];
        string urlfile = array[0];
        string code = Request.QueryString["code"]; 
        QinMingWeixinOAuth oauth = new QinMingWeixinOAuth();
        string readopenid = oauth.GetOpenId(code);
        Response.Redirect(urlfile+"?open_id="+openid + "&read_open_id=" + readopenid );
    }

}
   

