﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using QinMing.Config;
using QinMing.Sms;
using QinMing.Tools;

namespace Jjlm
{
	public partial class DisplayOneUser : System.Web.UI.Page
	{

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				lbopenid.Text = Request.QueryString["open_id"];  //此处需增加防sql注入判断

				if(QinMing.Tools.QinMingTools.IsHasSQLInject(lbopenid.Text))
				{
					Response.Write("请勿非法尝试！");
					Response.End();
				}
				
				SqlConnection Conn = new SqlConnection(QinMingConfig.DatabaseConnStr);
				Conn.Open();
				SqlCommand Comm = new SqlCommand();
				Comm.Connection = Conn;
				SqlDataReader dr;
				Comm.CommandText = "select * from weixin_user_info where open_id='" + lbopenid.Text + "'";
				dr = Comm.ExecuteReader();
				if (dr.Read())
				{
					imgHead.Src = dr["headimgurl"].ToString();
					divInfo.InnerHtml = "</br></br>" + dr["open_id"].ToString() + "</br></br>"
					    + "昵称：" + dr["nickname"].ToString() + "</br></br>"
						+ "省分：" + dr["province"].ToString() + "</br></br>"
						+ "省分：" + dr["city"].ToString() + "</br></br>"
						+ "推荐人号码：" + dr["guishu_mobile"].ToString() + "</br></br>";
					lbTuijianren.Text = dr["guishu_mobile"].ToString();
				}
				dr.Close();
				
				Comm.CommandText = "select * from weixin_bind_mobile_all where mobile='" + lbTuijianren.Text + "'";
				dr = Comm.ExecuteReader();
				if (dr.Read())
				{
					divInfo.InnerHtml = divInfo.InnerHtml + "推荐人姓名：" + dr["user_name"].ToString();
				}
				dr.Close();
				
				if (Conn.State == ConnectionState.Open)
				{
					Conn.Close();
					Conn.Dispose();
				}
			}
		}
		
	}
	
}