﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WeixinPayRecvJsApiTest.aspx.cs" Inherits="Jjlm.WeixinPayRecvJsApiTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="mui-master/dist/css/mui.min.css">
    <script src="mui-master/dist/js/mui.min.js"></script>
    <script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
    <title>微信支付JsApi收款测试</title>
</head>

<style>
    img{
        width: 100%;
        display: block;
    }
    body{
        margin:0;padding:0;
        background-image: url(img/maidanglao/bg.jpg);
        background-repeat: no-repeat;
        background-size: 100% 100%;
    }
    .line{
        display: flex;justify-content:space-between;height: 40px;margin: 0 20px;
    }
    .boline{
        border-bottom: 1px solid #ddd;
    }
    .ti{
        font-size: 18px;
        line-height: 40px;
    }
    .text{
        font-size: 16px;
        line-height: 40px;
    }
</style>

<script type="text/javascript">

    //调用微信JS api 支付
    function jsApiCall()
    {
        WeixinJSBridge.invoke(
        'getBrandWCPayRequest',
        <%=wxJsApiParam%>,//json串
        function (res) {
            /*if(res.err_msg == "get_brand_wcpay_request:ok" )//支付成功返回
            {
                alert("支付成功");
            }
            else
            {
                alert("支付失败");
            }*/
            //WeixinJSBridge.log(res.err_msg);
            //alert(res.err_code +"|"+ res.err_desc +"|" + res.err_msg);
        });
    }

    function callpay()
    {
        if (typeof WeixinJSBridge == "undefined")
        {
            if (document.addEventListener)
            {
                document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
            }
            else if (document.attachEvent)
            {
                document.attachEvent('WeixinJSBridgeReady', jsApiCall);
                document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
            }
        }
        else
        {
            jsApiCall();
        }
        
    }
        
</script>

<body>
<div style="width: 100%;background:#555;color:#fff;font-size:20px;height: 40px;line-height: 40px;text-align: center;">
    收银台
</div>
<form id="form1" runat="server">
    <div>
        <div style="margin-top:10px;background: #fff;">
            <div class="line boline">
                <span class="ti">订单号：</span>
                <asp:Label runat="server" id="lbBillNo" Text="" Visible="true" class="text" />
            </div>
            <div class="line boline">
                <span class="ti">商品名称：</span>
                <asp:Label runat="server" id="lbproductname" Text="" Visible="true" class="text" />
            </div>
            <div class="line boline">
                <span class="ti">商品数量： </span>
                <asp:Label runat="server" id="product_num" Text="1" Visible="true" class="text" />
            </div>
            <div class="line">
                 <span class="ti">支付金额：</span>
                 <asp:Label runat="server" id="lbtotal_fee" Text="0.01" Visible="true" class="text" style="color: crimson;" />
            </div>
        </div>
		<div style="margin: 10px auto;">
			<img src="img/wxzf.jpg" alt="" style="width: 100%;">
        </div>
        <div style="margin: 10px auto;width: 90%;height: 40px;background: #20a91d;border-radius:10px;text-align: center;" runat="server" id="divBtn">
            <asp:Button ID="submit" runat="server" Text="立即支付"  width="100%" Style="background: #20a91d;border-width:0px;color: #fff;line-height: 40px;font-size: 20px;" OnClick="btnCallPayClick"/>
        </div>
        </br>
		
		<center>
            <asp:HyperLink ID="hlSeeOrder" runat="server" Visible="false">
                <asp:Label runat="server" id="lbResult" Text="点击此处查看订单"></asp:Label>
            </asp:HyperLink>
        </center>

		<asp:Label runat="server" id="lbopenid" Visible="false"></asp:Label>

    </div>
</form>
</body>
</html>