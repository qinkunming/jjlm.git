﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;  
using Newtonsoft.Json.Converters; 
using Newtonsoft.Json.Linq; 
using QinMing.Config;
using QinMing.WeixinContainer;

namespace Jjlm
{
	
	public partial class MenuSet : System.Web.UI.Page
	{
		string strConn = QinMingConfig.DatabaseConnStr;
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!Page.IsPostBack)
			{
                //从数据表中读取最近一次菜单配置信息
				SqlConnection Conn = new SqlConnection(strConn);
				Conn.Open();
				SqlCommand Comm = new SqlCommand();
				Comm.Connection = Conn;
				SqlDataReader dr;
				   
				Comm.CommandText="select * from weixin_menu where caidan_id='1' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxCN1.Text=dr["caidan_name"].ToString();
					TbxKOU1.Text=dr["lu_or_ek"].ToString();
					ddCDLX1.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='11' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC11N.Text=dr["caidan_name"].ToString();
					TbxZ11KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD11LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='12' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC12N.Text=dr["caidan_name"].ToString();
					TbxZ12KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD12LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='13' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC13N.Text=dr["caidan_name"].ToString();
					TbxZ13KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD13LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='14' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC14N.Text=dr["caidan_name"].ToString();
					TbxZ14KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD14LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='15' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC15N.Text=dr["caidan_name"].ToString();
					TbxZ15KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD15LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='2' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxCN2.Text=dr["caidan_name"].ToString();
					TbxKOU2.Text=dr["lu_or_ek"].ToString();
					ddCDLX2.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='21' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC21N.Text=dr["caidan_name"].ToString();
					TbxZ21KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD21LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='22' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC22N.Text=dr["caidan_name"].ToString();
					TbxZ22KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD22LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='23' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC23N.Text=dr["caidan_name"].ToString();
					TbxZ23KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD23LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='24' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC24N.Text=dr["caidan_name"].ToString();
					TbxZ24KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD24LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='25' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC25N.Text=dr["caidan_name"].ToString();
					TbxZ25KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD25LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='3' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxCN3.Text=dr["caidan_name"].ToString();
					TbxKOU3.Text=dr["lu_or_ek"].ToString();
					ddCDLX3.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='31' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC31N.Text=dr["caidan_name"].ToString();
					TbxZ31KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD31LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='32' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC32N.Text=dr["caidan_name"].ToString();
					TbxZ32KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD32LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='33' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC33N.Text=dr["caidan_name"].ToString();
					TbxZ33KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD33LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='34' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC34N.Text=dr["caidan_name"].ToString();
					TbxZ34KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD34LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Comm.CommandText="select * from weixin_menu where caidan_id='35' and isuse='0'";
				dr=Comm.ExecuteReader();
				if(dr.Read())
				{
					TbxZC35N.Text=dr["caidan_name"].ToString();
					TbxZ35KOU.Text=dr["lu_or_ek"].ToString();
					ddZCD35LX.Items.FindByText(dr["caidan_type"].ToString()).Selected=true;
				}
				dr.Close();
				Conn.Close();
				Conn.Dispose();
			}
		}
		
		//获取基础access_token
		public void  GetToken()
		{
			QinMingWeixinContainer gt = new QinMingWeixinContainer();
			lblAC.Text = gt.GetAccessToken();
			lblAC.Visible = true;
		}
		
		//向微信服务器发起post请求，json格式
		public string GetPage(string posturl, string postData)
		{
		  
			Stream outstream = null;
			Stream instream = null;
			StreamReader sr = null;
			HttpWebResponse response = null;
			HttpWebRequest request = null;
			Encoding encoding = Encoding.UTF8;
			byte[] data = encoding.GetBytes(postData);
			// 准备请求...
			try
			{
				// 设置参数
				request = WebRequest.Create(posturl) as HttpWebRequest;
				CookieContainer cookieContainer = new CookieContainer();
				request.CookieContainer = cookieContainer;
				request.AllowAutoRedirect = true;
				request.Method = "POST";
				request.ContentType = "application/x-www-form-urlencoded";
				request.ContentLength = data.Length;
				outstream = request.GetRequestStream();
				outstream.Write(data, 0, data.Length);
				outstream.Close();
				//发送请求并获取相应回应数据
				response = request.GetResponse() as HttpWebResponse;
				//直到request.GetResponse()程序才开始向目标网页发送Post请求
				instream = response.GetResponseStream();
				sr = new StreamReader(instream, encoding);
				//返回结果网页（html）代码
				string content = sr.ReadToEnd();
				string err = string.Empty;
				return content;
			}
			catch (Exception ex)
			{
				string err = ex.Message;
				Response.Write(err);
				return string.Empty;
			}
		}
		
		//设置菜单，构建json格式数据
		public void json(object sencer,EventArgs e)
		{
			//防sql注入判断，可用参数法代替
			if(QinMing.Tools.QinMingTools.IsHasSQLInject(TbxCN1.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(ddCDLX1.SelectedItem.Value) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxKOU1.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC11N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ11KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC12N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ12KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC13N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ13KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC14N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ14KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC15N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ15KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxCN2.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(ddCDLX2.SelectedItem.Value)|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxKOU2.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC21N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ21KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC22N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ22KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC23N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ23KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC24N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ24KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC25N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ25KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxCN3.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(ddCDLX3.SelectedItem.Value)|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxKOU3.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC31N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ31KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC32N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ32KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC33N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ33KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC34N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ34KOU.Text)
				|| QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZC35N.Text) || QinMing.Tools.QinMingTools.IsHasSQLInject(TbxZ35KOU.Text)
			    )
			{
				Response.Write("请勿非法尝试！");
				Response.End();
			}

			GetToken();
			
			string weixin1="";
			weixin1 += "{\n";
			weixin1 += "\"button\":[\n";
			if(ddCDLX1.SelectedItem.Value=="click")
			{
			  weixin1 += "{\n";
			  weixin1 += "\"name\":\""+ TbxCN1.Text +" \",\n";
			  weixin1 += "\"type\":\""+ ddCDLX1.SelectedItem.Value +"\",\n";
			  weixin1 += "\"key\":\""+ TbxKOU1.Text +"\"\n";
			  weixin1 += "}\n";
			}
			if(ddCDLX1.SelectedItem.Value=="view")
			{
			  weixin1 += "{\n";
			  weixin1 += "\"name\":\""+ TbxCN1.Text +" \",\n";
			  weixin1 += "\"type\":\""+ ddCDLX1.SelectedItem.Value +"\",\n";
			  weixin1 += "\"url\":\""+ TbxKOU1.Text +"\"\n";
			  weixin1 += "}\n";
			}
			if(ddCDLX1.SelectedItem.Value=="父节点")
			{
			  weixin1 += "{\n";
			  weixin1 += "\"name\":\""+ TbxCN1.Text +" \",\n";
			  weixin1 += "\"sub_button\":[\n";
			  if(TbxZC11N.Text!=""&&TbxZ11KOU.Text!="")
			  {
				  if(ddZCD11LX.SelectedItem.Value=="click")
				  {
					  weixin1 += "{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC11N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ11KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD11LX.SelectedItem.Value=="view")
				  {
					  weixin1 += "{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC11N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ11KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  if(TbxZC12N.Text!=""&&TbxZ12KOU.Text!="")
			  {
				  if(ddZCD12LX.SelectedItem.Value=="click")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC12N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ12KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD12LX.SelectedItem.Value=="view")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC12N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ12KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  if(TbxZC13N.Text!=""&&TbxZ13KOU.Text!="")
			  {
				  if(ddZCD13LX.SelectedItem.Value=="click")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC13N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ13KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD13LX.SelectedItem.Value=="view")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC13N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ13KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  if(TbxZC14N.Text!=""&&TbxZ14KOU.Text!="")
			  {
				  if(ddZCD14LX.SelectedItem.Value=="click")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC14N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ14KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD14LX.SelectedItem.Value=="view")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC14N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ14KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  if(TbxZC15N.Text!=""&&TbxZ15KOU.Text!="")
			  {
				  if(ddZCD15LX.SelectedItem.Value=="click")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC15N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ15KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD15LX.SelectedItem.Value=="view")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC15N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ15KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  weixin1 += "]\n";
			  weixin1 += "}\n";
			}
			if(ddCDLX2.SelectedItem.Value=="click")
			{
			  weixin1 += ",{\n";
			  weixin1 += "\"name\":\""+ TbxCN2.Text +" \",\n";
			  weixin1 += "\"type\":\""+ ddCDLX2.SelectedItem.Value +"\",\n";
			  weixin1 += "\"key\":\""+ TbxKOU2.Text +"\"\n";
			  weixin1 += "}\n";
			}
			if(ddCDLX2.SelectedItem.Value=="view")
			{
			  weixin1 += ",{\n";
			  weixin1 += "\"name\":\""+ TbxCN2.Text +" \",\n";
			  weixin1 += "\"type\":\""+ ddCDLX2.SelectedItem.Value +"\",\n";
			  weixin1 += "\"url\":\""+ TbxKOU2.Text +"\"\n";
			  weixin1 += "}\n";
			  
			}
			if(ddCDLX2.SelectedItem.Value=="父节点")
			{
			  weixin1 += ",{\n";
			  weixin1 += "\"name\":\""+ TbxCN2.Text +" \",\n";
			  weixin1 += "\"sub_button\":[\n";
			  if(TbxZC21N.Text!=""&&TbxZ21KOU.Text!="")
			  {
				  if(ddZCD21LX.SelectedItem.Value=="click")
				  {
					  weixin1 += "{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC21N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ21KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD21LX.SelectedItem.Value=="view")
				  {
					  weixin1 += "{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC21N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ21KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  if(TbxZC22N.Text!=""&&TbxZ22KOU.Text!="")
			  {
				  if(ddZCD22LX.SelectedItem.Value=="click")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC22N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ22KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD22LX.SelectedItem.Value=="view")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC22N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ22KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  if(TbxZC23N.Text!=""&&TbxZ23KOU.Text!="")
			  {
				  if(ddZCD23LX.SelectedItem.Value=="click")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC23N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ23KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD23LX.SelectedItem.Value=="view")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC23N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ23KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  if(TbxZC24N.Text!=""&&TbxZ24KOU.Text!="")
			  {
				  if(ddZCD24LX.SelectedItem.Value=="click")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC24N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ24KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD24LX.SelectedItem.Value=="view")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC24N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ24KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  if(TbxZC25N.Text!=""&&TbxZ25KOU.Text!="")
			  {
				  if(ddZCD25LX.SelectedItem.Value=="click")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC25N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ25KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD25LX.SelectedItem.Value=="view")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC25N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ25KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  weixin1 += "]\n";
			  weixin1 += "}\n";
			}
			if(ddCDLX3.SelectedItem.Value=="click")
			{
			  weixin1 += ",{\n";
			  weixin1 += "\"name\":\""+ TbxCN3.Text +" \",\n";
			  weixin1 += "\"type\":\""+ ddCDLX3.SelectedItem.Value +"\",\n";
			  weixin1 += "\"key\":\""+ TbxKOU3.Text +"\"\n";
			  weixin1 += "}\n";
			}
			if(ddCDLX3.SelectedItem.Value=="view")
			{
			  weixin1 += ",{\n";
			  weixin1 += "\"name\":\""+ TbxCN3.Text +" \",\n";
			  weixin1 += "\"type\":\""+ ddCDLX3.SelectedItem.Value +"\",\n";
			  weixin1 += "\"url\":\""+ TbxKOU3.Text +"\"\n";
			  weixin1 += "}\n";
			  
			}
			if(ddCDLX3.SelectedItem.Value=="父节点")
			{
			  weixin1 += ",{\n";
			  weixin1 += "\"name\":\""+ TbxCN3.Text +" \",\n";
			  weixin1 += "\"sub_button\":[\n";
			  if(TbxZC31N.Text!=""&&TbxZ31KOU.Text!="")
			  {
				  if(ddZCD31LX.SelectedItem.Value=="click")
				  {
					  weixin1 += "{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC31N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ31KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD31LX.SelectedItem.Value=="view")
				  {
					  weixin1 += "{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC31N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ31KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  if(TbxZC32N.Text!=""&&TbxZ32KOU.Text!="")
			  {
				  if(ddZCD32LX.SelectedItem.Value=="click")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC32N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ32KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD32LX.SelectedItem.Value=="view")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC32N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ32KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  if(TbxZC33N.Text!=""&&TbxZ33KOU.Text!="")
			  {
				  if(ddZCD33LX.SelectedItem.Value=="click")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC33N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ33KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD33LX.SelectedItem.Value=="view")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC33N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ33KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  if(TbxZC34N.Text!=""&&TbxZ34KOU.Text!="")
			  {
				  if(ddZCD34LX.SelectedItem.Value=="click")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC34N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ34KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD34LX.SelectedItem.Value=="view")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC34N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ34KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  if(TbxZC35N.Text!=""&&TbxZ35KOU.Text!="")
			  {
				  if(ddZCD35LX.SelectedItem.Value=="click")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"click\",\n";
					  weixin1 += "\"name\":\""+ TbxZC35N.Text +"\",\n";
					  weixin1 += "\"key\":\""+ TbxZ35KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
				  if(ddZCD35LX.SelectedItem.Value=="view")
				  {
					  weixin1 += ",{\n";
					  weixin1 += "\"type\":\"view\",\n";
					  weixin1 += "\"name\":\""+ TbxZC35N.Text +"\",\n";
					  weixin1 += "\"url\":\""+ TbxZ35KOU.Text +"\"\n";
					  weixin1 += "}\n";
				  }
			  }
			  weixin1 += "]\n";
			  weixin1 += "}\n";
			}
			weixin1 += "]\n";
			weixin1 += "}\n";
			string i = GetPage("https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+lblAC.Text+"", weixin1);
			lblAC.Text += i;
			lblAC.Text += weixin1;
			lblAC.Visible = true;
			
			SqlConnection Conn = new SqlConnection(strConn);
			Conn.Open();
			SqlCommand Comm = new SqlCommand();
			Comm.Connection = Conn;

			Comm.CommandText="update weixin_menu set isuse='1' where isuse='0'";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('1','"+ TbxCN1.Text +"','"+ ddCDLX1.SelectedItem.Value +"','','1','"+ TbxKOU1.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('11','"+ TbxZC11N.Text +"','"+ ddZCD11LX.SelectedItem.Value +"','1','2','"+ TbxZ11KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('12','"+ TbxZC12N.Text +"','"+ ddZCD12LX.SelectedItem.Value +"','1','2','"+ TbxZ12KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('13','"+ TbxZC13N.Text +"','"+ ddZCD13LX.SelectedItem.Value +"','1','2','"+ TbxZ13KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('14','"+ TbxZC14N.Text +"','"+ ddZCD14LX.SelectedItem.Value +"','1','2','"+ TbxZ14KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('15','"+ TbxZC15N.Text +"','"+ ddZCD15LX.SelectedItem.Value +"','1','2','"+ TbxZ15KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('2','"+ TbxCN2.Text +"','"+ ddCDLX2.SelectedItem.Value +"','','1','"+ TbxKOU2.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('21','"+ TbxZC21N.Text +"','"+ ddZCD21LX.SelectedItem.Value +"','2','2','"+ TbxZ21KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('22','"+ TbxZC22N.Text +"','"+ ddZCD22LX.SelectedItem.Value +"','2','2','"+ TbxZ22KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('23','"+ TbxZC23N.Text +"','"+ ddZCD23LX.SelectedItem.Value +"','2','2','"+ TbxZ23KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('24','"+ TbxZC24N.Text +"','"+ ddZCD24LX.SelectedItem.Value +"','2','2','"+ TbxZ24KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('25','"+ TbxZC25N.Text +"','"+ ddZCD25LX.SelectedItem.Value +"','2','2','"+ TbxZ25KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('3','"+ TbxCN3.Text +"','"+ ddCDLX3.SelectedItem.Value +"','','1','"+ TbxKOU3.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('31','"+ TbxZC31N.Text +"','"+ ddZCD31LX.SelectedItem.Value +"','3','2','"+ TbxZ31KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('32','"+ TbxZC32N.Text +"','"+ ddZCD32LX.SelectedItem.Value +"','3','2','"+ TbxZ32KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('33','"+ TbxZC33N.Text +"','"+ ddZCD33LX.SelectedItem.Value +"','3','2','"+ TbxZ33KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('34','"+ TbxZC34N.Text +"','"+ ddZCD34LX.SelectedItem.Value +"','3','2','"+ TbxZ34KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();
			Comm.CommandText="insert into weixin_menu(caidan_id,caidan_name,caidan_type,parentid,index_level,lu_or_ek,createtime,isuse)"
							+"values('35','"+ TbxZC35N.Text +"','"+ ddZCD35LX.SelectedItem.Value +"','3','2','"+ TbxZ35KOU.Text +"',getdate(),'0')";
			Comm.ExecuteScalar();

			Conn.Close();
			Conn.Dispose();
		}
	}
}