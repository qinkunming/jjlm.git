using System;
using System.Web;
using QinMing.Config;
using QinMing.WeixinOAuth;

namespace Jjlm
{
	public partial class RedirectWithOpenIdFirst : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			string final_url = Request.QueryString["final_url"]; 
			string appid = QinMingConfig.Weixin_AppId;
			string redirect_url = QinMingConfig.Url_WebServer + "/weixin/RedirectWithOpenIdSecond.aspx" + "?final_url=" + final_url;
			QinMingWeixinOAuth oauth = new QinMingWeixinOAuth();
			string newurl = oauth.GetCodeBase(appid, redirect_url);
			Response.Redirect(newurl);
		}
	}
}   

